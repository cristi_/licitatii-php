<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Storage;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showMembru(Request $request, $id)
    {
        $licitatii = array();
        $licitatii_ids = null;
        if ($request->user()->admin) {
            $membru = DB::select('select * from users where id= :id', ['id' => $id]);
            if ($membru) {
                $membru = $membru[0];

                if ($membru->initiator) {
                    // pentru initiatori
                    $licitatii = DB::select('select * from licitatii where denumire_initiator = :name', ['name' => $membru->name]);
                } else {
                    $licitatii_ids = json_decode($membru->licitatii_participant, true);
                }

                if ($licitatii_ids != null) { // pentru furnizori
                    foreach ($licitatii_ids as $licitatie_id) {
                        array_push($licitatii, DB::select('select * from licitatii where id = :id', ['id' => $licitatie_id])[0]);
                    }
                }

                if ($membru->initiator) {
                    $detalii_initiator = DB::select('select * from initiator where user_id = :id', ['id' => $id]);
                    if ($detalii_initiator) {
                        $detalii_initiator = $detalii_initiator[0];
                    }

                    return view('showMembru')->with('membru', $membru)->with('licitatii', $licitatii)->with('detalii_initiator', $detalii_initiator);
                } else {
                    return view('showMembru')->with('membru', $membru)->with('licitatii', $licitatii);
                }
            } else {
                return redirect('/');
            }
        } elseif ($request->user()->id == $id) {
            $membru = DB::select('select * from users where id= :id', ['id' => $id])[0];

            if ($membru->initiator) {
                // pentru initiatori
                $licitatii = DB::select('select * from licitatii where denumire_initiator = :name', ['name' => $membru->name]);
            } else {
                $licitatii_ids = json_decode($membru->licitatii_participant, true);
            }

            if ($licitatii_ids != null) { // pentru furnizori
                foreach ($licitatii_ids as $licitatie_id) {
                    array_push($licitatii, DB::select('select * from licitatii where id = :id', ['id' => $licitatie_id])[0]);
                }
            }

            if ($membru->initiator) {
                $detalii_initiator = DB::select('select * from initiator where user_id = :id', ['id' => $id])[0];

                return view('showMembru')->with('membru', $membru)->with('licitatii', $licitatii)->with('detalii_initiator', $detalii_initiator);
            } else {
                return view('showMembru')->with('membru', $membru)->with('licitatii', $licitatii);
            }
        } else {
            return redirect('/');
        }
    }

    public function editMembru(Request $request, $id = null)
    {
        if ($id !== null) {
            $membru = DB::select('select * from users where id= :id', ['id' => $id]);

            return view('editMembru')->with('membru', $membru[0])->with('success', false);
        } else {
            $membru = DB::select('select * from users where id= :id', ['id' => $request->user()->id]);

            return view('editMembru')->with('membru', $membru[0])->with('success', false);
        }
    }

    public function stergeSolicitareMembru(Request $request, $id)
    {
        if (Auth::user()->admin || Auth::user()->id == $id) {
            DB::update('UPDATE users SET
                solicitare = false,
                solicitare_email = "",
                solicitare_adresa = "",
                solicitare_cui = "",
                solicitare_contact = "",
                solicitare_alte = ""
                WHERE id = ?',
                [$id]
            );
        }

        return redirect('/membru/'.$id);
    }

    public function aprobaSolicitareMembru(Request $request, $id)
    {
        if (Auth::user()->admin || Auth::user()->id == $id) {
            DB::update('UPDATE users SET
                email = solicitare_email,
                adresa = solicitare_adresa,
                cui = solicitare_cui,
                contact = solicitare_contact,
                telefon = solicitare_telefon,
                alte = solicitare_alte,
                solicitare = false,
                solicitare_email = "",
                solicitare_adresa = "",
                solicitare_cui = "",
                solicitare_contact = "",
                solicitare_alte = ""
                WHERE id = ?',
                [$id]
            );
        }

        return redirect('/membru/'.$id);
    }

    public function updateMembru(Request $request, $id = null)
    {
        if (Auth::user()->admin) {
            if ($request->has('parola')) {
                DB::update('UPDATE users SET password = ?, WHERE id = ?', [bcrypt($request->input('parola')), isset($id) ? $id : $request->user()->id]);
            }
            DB::update('UPDATE users SET initiator = ?, admin = ?, activ = ?, name = ?, email = ?, adresa = ?, cui = ?, contact = ?, telefon = ?, alte = ? WHERE id = ?',
                [
                    isset($id) ? $request->input('initiator') : $request->user()->initiator,
                    $request->input('admin'),
                    isset($id) ? $request->input('activ') : true,
                    $request->input('name'),
                    $request->input('email'),
                    $request->input('adresa'),
                    $request->input('cui'),
                    $request->input('contact'),
                    $request->input('telefon'),
                    $request->input('alte'),
                    isset($id) ? $id : $request->user()->id,
                ]); 			
        } else {
            DB::update('UPDATE users SET solicitare_email = ?, solicitare_adresa = ?, solicitare_cui = ?, solicitare_contact = ?, solicitare_telefon = ?, solicitare_alte = ?, solicitare = true WHERE id = ?',
            [
                $request->input('email'),
                $request->input('adresa'),
                $request->input('cui'),
                $request->input('contact'),
                $request->input('telefon'),
                $request->input('alte'),
                $request->user()->id,
            ]); 			

            // incrementeaza numarul de notificari nevizualizate
            DB::update('update solicitari set solicitari_informatii_num = solicitari_informatii_num + 1 limit 1');
            // adauga notificare
            DB::insert('insert into solicitari_informatii (id_user, nume_user, link, actiune) values (?, ?, ?, ?)', [Auth::user()->id, Auth::user()->name, 'membru/'.Auth::user()->id.'/edit', 'personal']);
        }

        $membru = DB::select('select * from users where id = :id', ['id' => isset($id) ? $id : $request->user()->id]);
        return view('editMembru')->with('membru', $membru[0])->with('success', true);
    }

    public function deleteMembru(Request $request, $id)
    {
        if ($request->user()->admin)
        {
            DB::delete('DELETE FROM users where id = :id', ['id' => $id]);
            return redirect('/');
        } else {
            return redirect('/');
        }
    }

    public function uploadFilesSolicitare(Request $request)
    {
        if ($request->hasFile('consum_anual') || $request->hasFile('consum_locuri')) {
            $membru = DB::select('select * from initiator where denumire = :denumire', ['denumire' => $request->user()->name]);
            $user = $membru[0]->user_id;
            $path = 'files/membrii/';
            $filename_prefix = $user.'_';

            if ($request->hasFile('consum_locuri') && $request->file('consum_locuri')->isValid()) {
                $consum_locuri = $request->file('consum_locuri');
                $file_type = 'consum_locuri_';
                $consum_locuri_name = $filename_prefix . $file_type . $consum_locuri->getClientOriginalName();
                $consum_locuri->move($path, $consum_locuri_name);
                DB::update('UPDATE initiator SET solicitare_consum_locuri = ? WHERE user_id = ?', [$path . $consum_locuri_name, $user]);
            }

            if ($request->hasFile('consum_anual') && $request->file('consum_anual')->isValid()) {
                $consum_anual = $request->file('consum_anual');
                $file_type = 'consum_anual_';
                $consum_anual_name = $filename_prefix . $file_type . $consum_anual->getClientOriginalName();
                $consum_anual->move($path, $consum_anual_name);
                DB::update('UPDATE initiator SET solicitare_consum_anual = ? WHERE user_id = ?', [$path . $consum_anual_name, $user]);
            }
        }
    }

    public function uploadFiles(Request $request)
    {
        if ($request->hasFile('consum_anual') || $request->hasFile('consum_locuri')) {
            $membru = DB::select('select * from initiator where denumire = :denumire', ['denumire' => $request->user()->name]);
            $user = $membru[0]->user_id;
            $path = 'files/membrii/';
            $filename_prefix = $user.'_';

            if ($request->hasFile('consum_locuri') && $request->file('consum_locuri')->isValid()) {
                $consum_locuri = $request->file('consum_locuri');
                $file_type = 'consum_locuri_';
                $consum_locuri_name = $filename_prefix . $file_type . $consum_locuri->getClientOriginalName();
                $consum_locuri->move($path, $consum_locuri_name);
                DB::update('UPDATE initiator SET consum_locuri = ? WHERE user_id = ?', [$path . $consum_locuri_name, $user]);
            }

            if ($request->hasFile('consum_anual') && $request->file('consum_anual')->isValid()) {
                $consum_anual = $request->file('consum_anual');
                $file_type = 'consum_anual_';
                $consum_anual_name = $filename_prefix . $file_type . $consum_anual->getClientOriginalName();
                $consum_anual->move($path, $consum_anual_name);
                DB::update('UPDATE initiator SET consum_anual = ? WHERE user_id = ?', [$path . $consum_anual_name, $user]);
            }
        }
    }

    public function aprobaSolicitareInitiator(Request $request, $id)
    {
        if (Auth::user()->admin || Auth::user()->id == $id) {
            DB::update('UPDATE initiator SET
                cantitate = solicitare_cantitate,
                nivel = solicitare_nivel,
                perioada_start = solicitare_perioada_start,
                perioada_sfarsit = solicitare_perioada_sfarsit,
                incepere = solicitare_incepere,
                termen = solicitare_termen,
                modalitate = solicitare_modalitate,
                program = solicitare_program,
                solicitare = false,
                solicitare_cantitate = "",
                solicitare_nivel = "",
                solicitare_perioada_start = "",
                solicitare_perioada_sfarsit = "",
                solicitare_incepere = "",
                solicitare_termen = "",
                solicitare_modalitate = "",
                solicitare_program = ""
                WHERE user_id = ?',
                [$id]
            );
        }

        return redirect('/membru/'.$id);
    }

    public function stergeSolicitareInitiator(Request $request, $id)
    {
        if (Auth::user()->admin || Auth::user()->id == $id) {
            DB::update('UPDATE initiator SET
                solicitare = false,
                solicitare_cantitate = "",
                solicitare_nivel = "",
                solicitare_perioada_start = "",
                solicitare_perioada_sfarsit = "",
                solicitare_incepere = "",
                solicitare_termen = "",
                solicitare_modalitate = "",
                solicitare_program = ""
                WHERE user_id = ?',
                [$id]
            );
        }

        return redirect('/membru/'.$id);
    }

    public function updateInitiator(Request $request) 
    {
        $existing = DB::select('select * from initiator where denumire = :denumire', ['denumire' => $request->input('denumire')]);

        if (Auth::user()->admin) {
            DB::update('UPDATE initiator SET cantitate = ?, nivel = ?, perioada_start = ?, perioada_sfarsit = ?, incepere = ?, termen = ?, modalitate = ?, program = ? WHERE denumire = ?', [$request->input('cantitate'), $request->input('nivel'), $request->input('perioada_start'), $request->input('perioada_sfarsit'), $request->input('incepere'), $request->input('termen'), $request->input('modalitate'), $request->input('program'), $request->input('denumire')]);
            $this->uploadFiles($request);
        } else {
            DB::update('UPDATE initiator SET solicitare_cantitate = ?, solicitare_nivel = ?, solicitare_perioada_start = ?, solicitare_perioada_sfarsit = ?, solicitare_incepere = ?, solicitare_termen = ?, solicitare_modalitate = ?, solicitare_program = ?, solicitare = true WHERE denumire = ?', [$request->input('cantitate'), $request->input('nivel'), $request->input('perioada_start'), $request->input('perioada_sfarsit'), $request->input('incepere'), $request->input('termen'), $request->input('modalitate'), $request->input('program'), $request->input('denumire')]);
            $this->uploadFilesSolicitare($request);
            // incrementeaza numarul de notificari nevizualizate
            DB::update('update solicitari set solicitari_informatii_num = solicitari_informatii_num + 1 limit 1');
            // adauga notificare
            DB::insert('insert into solicitari_informatii (id_user, nume_user, link, actiune) values (?, ?, ?, ?)', [Auth::user()->id, Auth::user()->name, 'membru/'.Auth::user()->id.'/editInitiator', 'initiator']);
        }

        $existing = DB::select('select * from initiator where denumire = :denumire', ['denumire' => $request->input('denumire')]);
        return view('editInitiator')->with('initiator', $existing[0])->with('success', true);
    }

    public function editInitiator(Request $request, $id)
    {
        $existing = DB::select('select * from initiator where user_id = :user_id', ['user_id' => $id]);
        return view('editInitiator')->with('initiator', $existing[0])->with('success', false);
    }

    public function listaMembriiFirst(Request $request, $opt = null)
    {
        if ($request->user()->admin) {
            if ($opt) {
                $membrii = DB::select('select * from users where initiator = :initiator limit ' . 10 . ' offset ' . 0, ['initiator' => $opt == 'initiatori' ? true : false ]);
            } else {
                $membrii = DB::select('select * from users limit ' . 10 . ' offset ' . 0);
            }

            return view('lista_membrii')->with('page', 0)->with('membrii', $membrii)->with('opt', $opt);
        } else {
            return redirect('/');
        }
    }

    public function listaMembrii(Request $request, $page)
    {
        if ($request->user()->admin) {
            if ($page < 0) {
                return redirect('lista_membrii');
            } else {
                $membrii = DB::select('select * from users limit ' . 10 . ' offset ' . $page * 10);

                if ($membrii) {
                    return view('lista_membrii')->with('page', $page)->with('membrii', $membrii);
                } else {
                    return redirect('membrii');
                }

            }
        } else {
            return redirect('/');
        }
    }

    public function listaOptMembrii(Request $request, $opt, $page)
    {
        if ($request->user()->admin) {
            if ($page < 0) {
                return redirect('lista_membrii');
            } else {
                $membrii = DB::select('select * from users where initiator = :initiator limit ' . 10 . ' offset ' . $page * 10, ['initiator' => $opt == 'initiatori' ? true : false ]);

                if ($membrii) {
                    return view('lista_membrii')->with('page', $page)->with('membrii', $membrii)->with('opt', $opt);
                } else {
                    return redirect('membrii');
                }

            }
        } else {
            return redirect('/');
        }
    }

    public function showLicitatie(Request $request, $id) 
    {
        $existing = DB::select('select * from licitatii where id = :id', ['id' => $id]);
        if ($existing) {
            $activa = true;
            $rezultat = false;
            $initiator = false;
            $participant = false;
            $participare_solicitata = false;
            $participanti_solicitare = '';
            $participanti = '';
            $utilizatori = '';
            $castigator = '';
            $oferte = '';

            // verifica daca licitatia este activa
            if( strtotime($existing[0]->data_incepere_licitatie .' '. $existing[0]->ora_incepere_licitatie) > time() ||
                strtotime($existing[0]->data_expirare_licitatie .' '. $existing[0]->ora_expirare_licitatie) < time() ){
                $activa = false;
            }

            // verifica daca userul este initiator
            if ($request->user()->name == $existing[0]->denumire_initiator) {
                $initiator = true;
            }

            // verifica daca exista participanti
            if ($existing[0]->participanti) {			
                // verifica daca userul este participant
                $participanti_licitatie = json_decode($existing[0]->participanti, true);
                if (in_array($request->user()->id, $participanti_licitatie)) {
                    $participant = true;
                }

                // stochaza datele participantilor intr-un array
                foreach ($participanti_licitatie as $key => $value) {
                    $participanti[$key] = DB::select('select * from users where id = :id', ['id' => $value])[0];
                }
            }

            // lista utilizatori excluzand initiatorii, adminii si participantii
            if ($request->user()->admin) {
                $utilizatori = DB::select('select * from users where admin = 0 AND initiator = 0');
                if ($participanti) {
                    foreach ($utilizatori as $key => $value) {
                        foreach ($participanti as $local_participant) {
                            if ($value->id == $local_participant->id) {
                                unset($utilizatori[$key]);
                            }
                        }
                    }
                }
            }

            // verifica daca exista solicitari de participare
            if ($existing[0]->participanti_solicitare) {			
                // verifica daca userul a solicitat participarea
                $participanti_solicitare_licitatie = json_decode($existing[0]->participanti_solicitare, true);
                if (in_array($request->user()->id, $participanti_solicitare_licitatie)) {
                    $participare_solicitata = true;
                }

                // stochaza datele solicitantilor intr-un array
                foreach ($participanti_solicitare_licitatie as $key => $value) {
                    $participanti_solicitare[$key] = DB::select('select * from users where id = :id', ['id' => $value])[0];
                }
            }

            // daca exista oferte, stochaza datele acestora intr-un array
            if ($existing[0]->oferte) {
                if ($participant) {
                    if ($existing[0]->live) {
                        $oferte = json_decode($existing[0]->oferte, true);
                    } else {
                        $toate_ofertele = json_decode($existing[0]->oferte, true);
                        if (array_search($request->user()->id, array_column($toate_ofertele, 'id')) !== false) {
                            $oferta_participant = $toate_ofertele[array_search($request->user()->id, array_column($toate_ofertele, 'id'))];
                            $oferte[0] = $oferta_participant;
                        }					
                    }
                } else {
                    $oferte = json_decode($existing[0]->oferte, true);
                }

                if ($initiator) {
                    $oferte = json_decode($existing[0]->oferte, true);
                }

                if ($oferte != '') {
                    foreach ($oferte as $key => $value) {
                        $oferte[$key]['nume'] = DB::select('select name from users where id = :id', ['id' => $value['id']])[0]->name;
                    }
                }
            }

            // verifica licitatia a luat sfarsit
            if (strtotime($existing[0]->data_expirare_licitatie .' '. $existing[0]->ora_expirare_licitatie) < time()) {
                $rezultat = true;
                if ($oferte != '') {
                    // alege cele mai recente oferte
                    $oferte_recente = array();
                    foreach ($oferte as $key_oferte => $val_oferte) {
                        if (in_array($val_oferte['id'], array_column($oferte_recente, 'id')) !== false) {
                            if ($val_oferte['data'] > array_column($oferte_recente, 'data')) {
                                $oferte_recente[array_search($val_oferte['id'], array_column($oferte_recente, 'id'))]['suma'] = $val_oferte['suma'];
                            }
                        } else {
                            array_push($oferte_recente, $val_oferte);
                        }
                    }

                    $oferta_castigatoare = array(
                        'nume' => '',
                        'id' => 0,
                        'suma' => PHP_INT_MAX
                    );

                    // alege cea mai mica oferta
                    foreach ($oferte_recente as $key => $val) {
                        if ($val['suma'] < $oferta_castigatoare['suma']) {
                            $oferta_castigatoare['nume'] = DB::select('select name from users where id = :id', ['id' => $val['id']])[0]->name;
                            $oferta_castigatoare['id'] = $val['id'];
                            $oferta_castigatoare['suma'] = $val['suma'];
                        }
                    }

                    $castigator = array(
                        'nume' => $oferta_castigatoare['nume'],
                        'id' => $oferta_castigatoare['id'],
                        'suma' => $oferta_castigatoare['suma']
                    );
                }
            }

            $id_initiator = DB::select('SELECT id FROM users WHERE name = ?', [$existing[0]->denumire_initiator])[0]->id;


            if ($existing[0]->live) {
                $licitatii = DB::select('SELECT * FROM licitatii WHERE aprobat = true ORDER BY id DESC LIMIT 10');
                    
                return view('licitatie_live')->with([
                    'participanti_solicitare' => $participanti_solicitare,
                    'participare_solicitata' => $participare_solicitata,
                    'id_initiator' => $id_initiator,
                    'participanti' => $participanti,
                    'utilizatori' => $utilizatori,
                    'participant' => $participant, 
                    'castigator' => $castigator,
                    'licitatie' => $existing[0],
                    'licitatii' => $licitatii,
                    'initiator' => $initiator, 
                    'rezultat' => $rezultat,
                    'oferte' => $oferte,
                    'activa' => $activa
                ]);
            } else {
                return view('licitatie_termen')->with([
                    'participanti_solicitare' => $participanti_solicitare,
                    'participare_solicitata' => $participare_solicitata,
                    'id_initiator' => $id_initiator,
                    'participanti' => $participanti,
                    'utilizatori' => $utilizatori,
                    'participant' => $participant, 
                    'castigator' => $castigator,
                    'licitatie' => $existing[0],
                    'initiator' => $initiator, 
                    'rezultat' => $rezultat,
                    'oferte' => $oferte,
                    'activa' => $activa
                ]);
            }

        } else {
            return redirect('/licitatii/');
        }
    }

    public function editInfo(Request $request)
    {
        $info = DB::select('SELECT * FROM info')[0];
        return view('editInfo')->with('info', $info->info);
    }

    public function updateInfo(Request $request)
    {
        DB::update('UPDATE info SET info = ? LIMIT 1', [$request->input('info')]);
        return redirect('/');
    }
}
