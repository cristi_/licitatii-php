<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use DB;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/licitatii';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'initiator' => '',
            'adresa' => '',
            'cui' => '',
            'contact' => '',
            'telefon' => '',
            'alte' => '',
        ];

        $validator = Validator::make($data, $rules);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if ($data['name'] == 'admin') {
            $admin = true;
        } else {
            $admin = false;
        }

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'initiator' => $data['initiator'],
            'admin' => $admin,
            'activ' => true,
            'password' => bcrypt($data['password']),
            'adresa' => $data['adresa'],
            'cui' => $data['cui'],
            'contact' => $data['contact'],
            'telefon' => $data['telefon'],
            'alte' => $data['alte'],
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->create($request->all());

        $email = $request->input('email');
        $users = DB::select('SELECT * FROM users WHERE email = :email', ['email' => $email]);
        $user = end($users);
        DB::insert('INSERT INTO initiator (user_id, denumire) VALUES (?, ?)', [$user->id, $request->input('name')]);

        return redirect($this->redirectPath());
    }
}
