<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Storage;

class LicitatiiController extends Controller
{
	public function listaLicitatiiFirst(Request $request)
	{
        if (Auth::user() && Auth::user()->admin) {
            $licitatii = DB::select('select * from licitatii where aprobat = true order by id desc limit ' . 10 . ' offset ' . 0);		
            return view('licitatii')->with('page', 0)->with('licitatii', $licitatii);
        } else {
            $sql = 'timestamp(data_expirare_licitatie, ora_expirare_licitatie) > now() and';
            $licitatii = DB::select('select * from licitatii where '.$sql.' aprobat = true order by id desc limit ' . 15 . ' offset ' . 0);		
            $numar_licitatii = count($licitatii);
            if ($numar_licitatii > 0) {
                $parti = array_chunk( $licitatii, ceil( count($licitatii) / 5 ));
                return view('licitatiiGrila')->with([
                    'licitatii' => $licitatii, 
                    'parti' => $parti,
                    'page' => 0
                ]);
            } else {
                return view('licitatiiGrila')->with([
                    'licitatii', $licitatii,
                    'parti' => array(),
                    'page' => 0
                ]);
            }
        }
	}

	public function listaLicitatii1(Request $request, $page) 
	{
		if ($page < 0) {
			return redirect('/licitatii');
		} else {
            if (Auth::user() && Auth::user()->admin) {
                $licitatii = DB::select('select * from licitatii where aprobat = true order by id desc limit ' . 10 . ' offset ' . 10 * $page);		
                return view('licitatii')->with([
                    'licitatii' => $licitatii,
                    'page' => $page
                ]);
            } else {
                $sql = 'timestamp(data_expirare_licitatie, ora_expirare_licitatie) > now() and';
                $licitatii = DB::select('select * from licitatii where '.$sql.' aprobat = true order by id desc limit ' . 15 . ' offset ' . 15 * $page);		
                $numar_licitatii = count($licitatii);
                if ($numar_licitatii > 0) {
                    $parti = array_chunk( $licitatii, ceil( count($licitatii) / 5 ));
                    return view('licitatiiGrila')->with([
                        'licitatii' => $licitatii,
                        'parti' => $parti,
                        'page' => $page
                    ]);
                } else {
                    return redirect('/licitatii');
                }
            }
		}
	}

	public function listaLicitatii3(Request $request, $type = null, $active = null, $page) 
	{
		if ($page < 0) {
			return redirect('/licitatii');
		} else {
            $limit = (Auth::user() && Auth::user()->admin) ? 10 : 15;

            if ($active == 'active'){
            	$sql = 'timestamp(data_incepere_licitatie, ora_incepere_licitatie) < now() and timestamp(data_expirare_licitatie, ora_expirare_licitatie) > now() and';
            } elseif ($active == 'inactive') {
                if (Auth::user()->admin) {
            	$sql = '(timestamp(data_incepere_licitatie, ora_incepere_licitatie) > now() or timestamp(data_expirare_licitatie, ora_expirare_licitatie) < now()) and';
                } else {
            	$sql = 'timestamp(data_incepere_licitatie, ora_incepere_licitatie) > now() and';
                }
            } else {
            	$sql = '';
            }

			if ($type == 'live') {
				$sql = 'select * from licitatii where '.$sql.' live = 1 and aprobat = 1 order by id desc limit ' . $limit . ' offset ' . $page * $limit;
			} elseif ($type == 'termen') {
				$sql = 'select * from licitatii where '.$sql.' live = 0 and aprobat = 1 order by id desc limit ' . $limit . ' offset ' . $page * $limit;
			} else {
				$sql = 'select * from licitatii where '.$sql.' aprobat = 1 order by id desc limit ' . $limit . ' offset ' . $page * $limit;
			}

            if (Auth::user() && Auth::user()->admin) {
                $licitatii = DB::select($sql);		
                return view('licitatii')->with([
                    'licitatii' => $licitatii,
                    'active'=> $active,
                    'page' => $page,
                    'type' => $type
                ]);
            } else {
                $licitatii = DB::select($sql);		
                $numar_licitatii = count($licitatii);
                if ($numar_licitatii > 0) {
                    $parti = array_chunk( $licitatii, ceil( count($licitatii) / 5 ));
                    return view('licitatiiGrila')->with([
                        'licitatii' => $licitatii,
                        'active' => $active,
                        'parti' => $parti,
                        'page' => $page,
                        'type' => $type
                    ]);
                } else {
                    $parti = array();
                    return view('licitatiiGrila')->with([
                        'licitatii' => $licitatii,
                        'active' => $active,
                        'parti' => $parti,
                        'page' => $page,
                        'type' => $type
                    ]);
                    // pastrez asta aici in caz de orice
                    if ($type == 'termen' && $page < 1) {
                        if ($active == 'active') { return redirect('/licitatii/tip/live/status/active/pagina/0'); }
                        elseif ($active == 'inactive') { return redirect('/licitatii/tip/live/status/inactive/pagina/0'); }
                        else { return redirect('/licitatii'); }
                    } elseif ($type == 'live' && $page < 1) {
                        if ($active == 'active') { return redirect('/licitatii/tip/termen/status/active/pagina/0'); }
                        elseif ($active == 'inactive') { return redirect('/licitatii/tip/termen/status/inactive/pagina/0'); }
                        else { return redirect('/licitatii'); }
                    } else {
                        return view('licitatii')->with([
                            'licitatii' => $licitatii,
                            'active' => $active,
                            'type' => $type,
                            'page' => $page
                        ]);
                    }
                }
            }
		}
	}

	public function solicitari(Request $request, $page = 0)
	{
		if ($page >= 0 && Auth::user() && Auth::user()->admin) {
            // reseteaza numarul de notificari nevizualizate
            DB::update('update solicitari set solicitari_licitatii_num = 0 limit 1');
            $licitatii = DB::select('select * from licitatii where aprobat = false order by id desc limit ' . 10 . ' offset ' . $page * 10);		
            return view('licitatii')->with([
                'location' => 'solicitari',
                'licitatii' => $licitatii,
                'page' => $page
            ]);
		} else { return redirect('/'); }
	}

	public function licitatiiMembru(Request $request, $id, $page = 0)
	{
		if ($page >= 0 && (Auth::user() && Auth::user()->admin || Auth::user()->id == $id)) {
            $limit = (Auth::user() && Auth::user()->admin) ? 10 : 15;

            $membru = DB::select('select * from users where id= :id', ['id' => $id])[0];
            $licitatii = array();
                
            if ($membru->initiator) {
                // pentru initiatori
                $licitatii = DB::select('select * from licitatii where denumire_initiator = :name order by id desc limit ' . $limit . ' offset ' . $page * $limit, ['name' => $membru->name]);
                $tip_membru = 'initiator';
            } else {
                // pentru furnizori
                $licitatii_ids = json_decode($membru->licitatii_participant, true);
                if ($licitatii_ids != null) { // daca furnizorul este participant la licitatii
                    $licitatii_ids = array_slice($licitatii_ids, $page * $limit, $limit);
                    foreach ($licitatii_ids as $licitatie_id) {
                        array_push($licitatii, DB::select('select * from licitatii where id = :id', ['id' => $licitatie_id])[0]);
                    }
                }
                $tip_membru = 'furnizor';
            }

            if (Auth::user()->admin) {
                return view('licitatii')->with([
                    'location' => 'membru/'.$id.'/licitatii/',
                    'tip_membru' => $tip_membru,
                    'licitatii' => $licitatii,
                    'membru' => $membru->name,
                    'page' => $page
                ]);
            } else {
                $numar_licitatii = count($licitatii);
                if ($numar_licitatii > 0) {
                    $parti = array_chunk( $licitatii, ceil( count($licitatii) / 5 ));
                    return view('licitatiiGrila')->with([
                        'location' => 'membru/'.$id.'/licitatii/',
                        'tip_membru' => $tip_membru,
                        'licitatii' => $licitatii,
                        'membru' => $membru->name,
                        'parti' => $parti,
                        'page' => $page
                    ]);
                } else {
                    return view('licitatiiGrila')->with([
                        'location' => 'membru/'.$id.'/licitatii/',
                        'tip_membru' => $tip_membru,
                        'licitatii' => $licitatii,
                        'membru' => $membru->name,
                        'parti' => array(),
                        'page' => $page
                    ]);
                }
            }
		} else { return redirect('/'); }
	}

	public function createLicitatie(Request $request) 
	{
        if (Auth::guest()) {
            return redirect('/');
        } elseif (Auth::user()->admin || $request->user()->initiator) {
            // lista utilizatori excluzand initiatorii, adminii si participantii
            if ($request->user()->admin) {
                $utilizatori = DB::select('select * from users where admin = 0 AND initiator = 1');
                return view('licitatie_noua')->with('utilizatori', $utilizatori);
            }
			return view('licitatie_noua');
		} else {
			return redirect('/');
		}
	}

	public function storeLicitatie(Request $request) 
	{
        if (Auth::guest()) { return redirect('/'); }
        elseif (Auth::user()->admin) {
            $id_initiator = DB::select('SELECT id FROM users WHERE name = :name', [$request->input('denumire_initiator')]);
            if ($id_initiator) {
                $id_initiator = $id_initiator[0]->id;
            } else {
                $id_initiator = 1;
            }
            if ($request->input('live')) {
                DB::insert('INSERT INTO licitatii (id_initiator,live,aprobat,denumire_licitatie,denumire_initiator,cantitate,tensiune,perioada_start,perioada_sfarsit,incepere,termen,modalitate,program,alte,data_incepere_licitatie,ora_incepere_licitatie,data_expirare_licitatie,ora_expirare_licitatie) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                [$id_initiator,$request->input('live'),true,$request->input('denumire_licitatie'),$request->input('denumire_initiator'),$request->input('cantitate'),$request->input('tensiune'),$request->input('perioada_start'),$request->input('perioada_sfarsit'),$request->input('incepere'),$request->input('termen'),$request->input('modalitate'),$request->input('program'),$request->input('alte'),$request->input('data_incepere_licitatie'),$request->input('ora_incepere_licitatie'),$request->input('data_incepere_licitatie'),$request->input('ora_incepere_licitatie')]);
            } else {
                DB::insert('INSERT INTO licitatii (id_initiator,live,aprobat,denumire_licitatie,denumire_initiator,cantitate,tensiune,perioada_start,perioada_sfarsit,incepere,termen,modalitate,program,alte,data_incepere_licitatie,ora_incepere_licitatie,data_expirare_licitatie,ora_expirare_licitatie) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                [$id_initiator,$request->input('live'),true,$request->input('denumire_licitatie'),$request->input('denumire_initiator'),$request->input('cantitate'),$request->input('tensiune'),$request->input('perioada_start'),$request->input('perioada_sfarsit'),$request->input('incepere'),$request->input('termen'),$request->input('modalitate'),$request->input('program'),$request->input('alte'),$request->input('data_incepere_licitatie'),$request->input('ora_incepere_licitatie'),$request->input('data_expirare_licitatie'),$request->input('ora_expirare_licitatie')]);
            }

            $licitatie = DB::select('select id from licitatii where denumire_licitatie = :denumire_licitatie', ['denumire_licitatie' => $request->input('denumire_licitatie')]);
            $licitatie = end($licitatie);
            $this->uploadFiles($request, $licitatie->id);

            return redirect('licitatie/'.$licitatie->id)->with('existing', $licitatie);

        } elseif (Auth::user()->initiator) {
            $id_initiator = Auth::user()->id;
            $denumire_licitatie = 'Solicitare licitatie';
            if ($request->input('live')) {
                DB::insert('INSERT INTO licitatii (id_initiator,live,aprobat,denumire_licitatie,denumire_initiator,cantitate,tensiune,perioada_start,perioada_sfarsit,incepere,termen,modalitate,program,alte,data_incepere_licitatie,ora_incepere_licitatie,data_expirare_licitatie,ora_expirare_licitatie) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                [$id_initiator,$request->input('live'),false,$denumire_licitatie,$request->input('denumire_initiator'),$request->input('cantitate'),$request->input('tensiune'),$request->input('perioada_start'),$request->input('perioada_sfarsit'),$request->input('incepere'),$request->input('termen'),$request->input('modalitate'),$request->input('program'),$request->input('alte'),$request->input('data_incepere_licitatie'),$request->input('ora_incepere_licitatie'),$request->input('data_incepere_licitatie'),$request->input('ora_incepere_licitatie')]);
            } else {
                DB::insert('INSERT INTO licitatii (id_initiator,live,aprobat,denumire_licitatie,denumire_initiator,cantitate,tensiune,perioada_start,perioada_sfarsit,incepere,termen,modalitate,program,alte,data_incepere_licitatie,ora_incepere_licitatie,data_expirare_licitatie,ora_expirare_licitatie) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                [$id_initiator,$request->input('live'),false,$denumire_licitatie,$request->input('denumire_initiator'),$request->input('cantitate'),$request->input('tensiune'),$request->input('perioada_start'),$request->input('perioada_sfarsit'),$request->input('incepere'),$request->input('termen'),$request->input('modalitate'),$request->input('program'),$request->input('alte'),$request->input('data_incepere_licitatie'),$request->input('ora_incepere_licitatie'),$request->input('data_expirare_licitatie'),$request->input('ora_expirare_licitatie')]);
            }

            $licitatie = DB::select('select id from licitatii where id_initiator = :id_initiator order by created_at desc limit 1', ['id_initiator' => $id_initiator]);
            $licitatie = end($licitatie);
            $this->uploadFiles($request, $licitatie->id);

            // incrementeaza numarul de notificari nevizualizate
            DB::update('update solicitari set solicitari_licitatii_num = solicitari_licitatii_num + 1 limit 1');

            return redirect('licitatie/'.$licitatie->id)->with('existing', $licitatie);
        } else { return redirect('/'); }
	}

    public function uploadFiles(Request $request, $id)
    {
        if ($request->hasFile('consum_anual') || $request->hasFile('consum_locuri')) {
            $path = 'files/licitatii/';
            $filename_prefix = $id.'_';

            if ($request->hasFile('consum_locuri') && $request->file('consum_locuri')->isValid()) {
                $consum_locuri = $request->file('consum_locuri');
                $file_type = 'consum_locuri_';
                $consum_locuri_name = $filename_prefix . $file_type . $consum_locuri->getClientOriginalName();
                $consum_locuri->move($path, $consum_locuri_name);
                DB::update('UPDATE licitatii SET consum_locuri = ? WHERE id = ?', [$path . $consum_locuri_name, $id]);
            }

            if ($request->hasFile('consum_anual') && $request->file('consum_anual')->isValid()) {
                $consum_anual = $request->file('consum_anual');
                $file_type = 'consum_anual_';
                $consum_anual_name = $filename_prefix . $file_type . $consum_anual->getClientOriginalName();
                $consum_anual->move($path, $consum_anual_name);
                DB::update('UPDATE initiator SET consum_anual = ? WHERE id = ?', [$path . $consum_anual_name, $id]);
            }
        }
    }

    public function uploadFilesSolicitare(Request $request, $id)
    {
        if ($request->hasFile('consum_anual') || $request->hasFile('consum_locuri')) {
            $path = 'files/licitatii/';
            $filename_prefix = $id.'_';

            if ($request->hasFile('consum_locuri') && $request->file('consum_locuri')->isValid()) {
                $consum_locuri = $request->file('consum_locuri');
                $file_type = 'consum_locuri_';
                $consum_locuri_name = $filename_prefix . $file_type . $consum_locuri->getClientOriginalName();
                $consum_locuri->move($path, $consum_locuri_name);
                DB::update('UPDATE licitatii SET solicitare_consum_locuri = ? WHERE id = ?', [$path . $consum_locuri_name, $id]);
            }

            if ($request->hasFile('consum_anual') && $request->file('consum_anual')->isValid()) {
                $consum_anual = $request->file('consum_anual');
                $file_type = 'consum_anual_';
                $consum_anual_name = $filename_prefix . $file_type . $consum_anual->getClientOriginalName();
                $consum_anual->move($path, $consum_anual_name);
                DB::update('UPDATE initiator SET solicitare_consum_anual = ? WHERE id = ?', [$path . $consum_anual_name, $id]);
            }
        }
    }

    public function editLicitatie(Request $request, $id) 
    {
        if (Auth::guest()) { return redirect('/'); } 
        elseif ($request->user()->admin || $request->user()->initiator) {
            $existing = DB::select('select * from licitatii where id = :id', ['id' => $id]);
            $utilizatori = DB::select('select * from users where initiator = 1 AND admin = 0');
            if ($existing) { 
                return view('licitatie_modificare')->with([
                    'utilizatori' => $utilizatori,
                    'licitatie' => $existing[0]
                ]); 
            }
        }
        return redirect('/');
    }

	public function updateLicitatie(Request $request, $id) 
	{
        if (Auth::guest()) { return redirect('/'); }
        elseif (Auth::user()->admin) {
            $id_initiator = DB::select('SELECT id FROM users WHERE name = :name', [$request->input('denumire_initiator')]);
            if ($id_initiator) {
                $id_initiator = $id_initiator[0]->id;
            } else {
                $id_initiator = 1;
            }
            DB::update('UPDATE licitatii SET id_initiator = ?, live = ?, denumire_licitatie = ?, denumire_initiator = ?, cantitate = ?, tensiune = ?, perioada_start = ?, perioada_sfarsit = ?, incepere = ?, termen = ?, modalitate = ?, program = ?, alte = ?, data_incepere_licitatie = ?, ora_incepere_licitatie = ?, data_expirare_licitatie = ?, ora_expirare_licitatie = ? WHERE id = ?',
                [ $id_initiator, $request->input('live'), $request->input('denumire_licitatie'), $request->input('denumire_initiator'), $request->input('cantitate'), $request->input('tensiune'), $request->input('perioada_start'),$request->input('perioada_sfarsit'), $request->input('incepere'), $request->input('termen'), $request->input('modalitate'), $request->input('program'), $request->input('alte'), $request->input('data_incepere_licitatie'), $request->input('ora_incepere_licitatie'), $request->input('data_expirare_licitatie'), $request->input('ora_expirare_licitatie'), $id ]
            );          

            $licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];

            if ($licitatie->consum_locuri != '' && Storage::exists($licitatie->consum_locuri)) {
                Storage::delete($licitatie->consum_locuri);
            }

            if ($licitatie->consum_anual != '' && Storage::exists($licitatie->consum_anual)) {
                Storage::delete($licitatie->consum_anual);
            }

            $this->uploadFiles($request, $id);
        
            return redirect('licitatie/'.$id)->with('existing', $licitatie);
        }
        elseif (Auth::user()->initiator) {
            $denumire_licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0]->denumire_licitatie;
            DB::update('UPDATE licitatii SET solicitare = true, solicitare_live = ?, solicitare_denumire_licitatie = ?, solicitare_cantitate = ?, solicitare_tensiune = ?, solicitare_perioada_start = ?, solicitare_perioada_sfarsit, solicitare_incepere = ?, solicitare_termen = ?, solicitare_modalitate = ?, solicitare_program = ?, solicitare_alte = ?, solicitare_data_incepere_licitatie = ?, solicitare_ora_incepere_licitatie = ?, solicitare_data_expirare_licitatie = ?, solicitare_ora_expirare_licitatie = ? WHERE id = ?',
                [ $request->input('live'), $denumire_licitatie, $request->input('cantitate'), $request->input('tensiune'), $request->input('perioada_start'), $request->input('perioada_sfarsit'), $request->input('incepere'), $request->input('termen'), $request->input('modalitate'), $request->input('program'), $request->input('alte'), $request->input('data_incepere_licitatie'), $request->input('ora_incepere_licitatie'), $request->input('data_expirare_licitatie'), $request->input('ora_expirare_licitatie'), $id ]
            );          

            $licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];

            if ($licitatie->consum_locuri != '' && Storage::exists($licitatie->consum_locuri)) {
                Storage::delete($licitatie->consum_locuri);
            }

            if ($licitatie->consum_anual != '' && Storage::exists($licitatie->consum_anual)) {
                Storage::delete($licitatie->consum_anual);
            }

            $this->uploadFilesSolicitare($request, $id);

            // incrementeaza numarul de notificari nevizualizate
            DB::update('update solicitari set solicitari_informatii_num = solicitari_informatii_num + 1 limit 1');
            // adauga notificare
            DB::insert('insert into solicitari_informatii (id_user, nume_user, link, actiune) values (?, ?, ?, ?)', [Auth::user()->id, Auth::user()->name, 'licitatie/'.$id.'/edit', 'licitatie']);
        
            return redirect('licitatie/'.$id)->with('existing', $licitatie);
        }
        else { return redirect('/'); }
	}

	public function deleteLicitatie(Request $request, $id) 
	{
		if ($request->user()->admin) {
            $licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];
            if ($licitatie->participanti) { // daca exista participanti
                // decodifica array-ul si sterge participantii
                $participanti = json_decode($licitatie->participanti, true);
                foreach($participanti as $id_utilizator) {
                    $utilizator = DB::select('select * from users where id = :id', ['id' => $id_utilizator]);
                    if ($utilizator) {
                        $utilizator = $utilizator[0];
                        // sterge id licitatie in lista licitatiilor la care userul participa
                        if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are licitatii
                            $licitatii_participant = json_decode($utilizator->licitatii_participant, true);
                            if (array_search($id, $licitatii_participant) !== false) { // verifica daca licitatia este in lista utilizatorului
                                unset($licitatii_participant[array_search($id, $licitatii_participant)]);
                                DB::update('UPDATE users SET licitatii_participant = ? WHERE id = ?', [ json_encode($licitatii_participant), $utilizator->id ]);
                            }
                        }
                    }
                }
            }
			DB::delete('DELETE FROM licitatii where id = :id', ['id' => $id]);
			return redirect('/');
		} else {
			return redirect('/');
		}
	}

    public function aprobaSolicitare(Request $request, $id)
    {
        if (Auth::user()->admin) {
            DB::update('UPDATE licitatii SET
                live = solicitare_live,
                denumire_licitatie = solicitare_denumire_licitatie,
                cantitate = solicitare_cantitate,
                tensiune = solicitare_tensiune,
                perioada_start = solicitare_perioada_start,
                perioada_sfarsit = solicitare_perioada_sfarsit,
                incepere = solicitare_incepere,
                termen = solicitare_termen,
                modalitate = solicitare_modalitate,
                program = solicitare_program,
                alte = solicitare_alte,
                data_incepere_licitatie = solicitare_data_incepere_licitatie,
                ora_incepere_licitatie = solicitare_ora_incepere_licitatie,
                data_expirare_licitatie = solicitare_data_expirare_licitatie,
                ora_expirare_licitatie = solicitare_ora_expirare_licitatie,
                solicitare = false,
                solicitare_live = live,
                solicitare_denumire_licitatie = "",
                solicitare_cantitate = "",
                solicitare_tensiune = "",
                solicitare_perioada_start = "",
                solicitare_perioada_sfarsit = "",
                solicitare_incepere = "",
                solicitare_termen = "",
                solicitare_modalitate = "",
                solicitare_program = "",
                solicitare_alte = "",
                solicitare_data_incepere_licitatie = "",
                solicitare_ora_incepere_licitatie = "",
                solicitare_data_expirare_licitatie = "",
                solicitare_ora_expirare_licitatie = ""
                WHERE id = ?',
                [$id]
            );
        }

        return redirect(url('/licitatie/'.$id));
    }

    public function stergeSolicitare(Request $request, $id)
    {
        $denumire_initiator = DB::select('SELECT denumire_initiator FROM licitatii WHERE id = ?', [$id])[0]->denumire_initiator;
        if (Auth::user()->admin || Auth::user()->name == $denumire_initiator) {
            DB::update('UPDATE licitatii SET
                solicitare = false,
                solicitare_live = live,
                solicitare_denumire_licitatie = "",
                solicitare_cantitate = "",
                solicitare_tensiune = "",
                solicitare_perioada_start = "",
                solicitare_perioada_sfarsit = "",
                solicitare_incepere = "",
                solicitare_termen = "",
                solicitare_modalitate = "",
                solicitare_program = "",
                solicitare_alte = "",
                solicitare_data_incepere_licitatie = "",
                solicitare_ora_incepere_licitatie = "",
                solicitare_data_expirare_licitatie = "",
                solicitare_ora_expirare_licitatie = ""
                WHERE id = ?',
                [$id]
            );
        }

        return redirect(url('/licitatie/'.$id));
    }
}
