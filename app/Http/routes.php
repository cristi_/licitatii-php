<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



//Route::get('/licitatii', function () {
//    return view('licitatii');
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
use Illuminate\Http\Request;

Route::group(['middleware' => 'web'], function () {
    Route::auth();

	Route::get('/', function () {
		#$licitatii = DB::select('SELECT * FROM licitatii WHERE aprobat = true ORDER BY id DESC LIMIT 10');
		#$info = DB::select('SELECT * FROM info');
			
		#return view('home')->with('licitatii', $licitatii)->with('info', $info);
        return redirect('/licitatii');
	});

	Route::get('/editeaza_informatiile', 'HomeController@editInfo');
	Route::post('/editeaza_informatiile', 'HomeController@updateInfo');

	Route::get('/detalii', 'HomeController@editMembru');
	Route::post('/detalii', 'HomeController@updateMembru');

	Route::get('/membrii/pagina/{page}', 'HomeController@listaMembrii');
	Route::get('/membrii/{opt?}', 'HomeController@listaMembriiFirst');
	Route::get('/membrii/{opt?}/pagina/{page}', 'HomeController@listaOptMembrii');

	Route::get('/membru/{id}', 'HomeController@showMembru');
	Route::get('/membru/{id}/delete', 'HomeController@deleteMembru');
	Route::get('/membru/{id}/edit', 'HomeController@editMembru');
	Route::get('/membru/{id}/editInitiator', 'HomeController@editInitiator');
	Route::post('/membru/{id}/edit', 'HomeController@updateMembru');
	Route::post('/membru/{id}/editInitiator', 'HomeController@updateInitiator');

	Route::post('/membru/{id}/aprobaMembru', 'HomeController@aprobaSolicitareMembru');
	Route::get('/membru/{id}/stergeSolicitareaMembru', 'HomeController@stergeSolicitareMembru');
	Route::post('/membru/{id}/aprobaInitiator', 'HomeController@aprobaSolicitareInitiator');
	Route::get('/membru/{id}/stergeSolicitareaInitiator', 'HomeController@stergeSolicitareInitiator');

	Route::get('/licitatie_noua', 'LicitatiiController@createLicitatie');
	Route::post('/licitatie_noua', 'LicitatiiController@storeLicitatie');

	Route::get('/licitatie/{id}', 'HomeController@showLicitatie');

	Route::get('/licitatie/{id}/edit', 'LicitatiiController@editLicitatie');
	Route::post('/licitatie/{id}/edit', 'LicitatiiController@updateLicitatie');
	Route::get('/licitatie/{id}/delete', 'LicitatiiController@deleteLicitatie');
	Route::post('/licitatie/{id}/aproba', 'LicitatiiController@aprobaSolicitare');
	Route::get('/licitatie/{id}/stergeSolicitare', 'LicitatiiController@stergeSolicitare');

	Route::get('/licitatii/', 'LicitatiiController@listaLicitatiiFirst');

	Route::get('/licitatii/pagina/{page}', 'LicitatiiController@listaLicitatii1');
	Route::get('/licitatii/tip/{type}/status/{activ}/pagina/{page}', 'LicitatiiController@listaLicitatii3');

	Route::get('/solicitari', 'LicitatiiController@solicitari');
	Route::get('/solicitari/pagina/{page}', 'LicitatiiController@solicitari');

	Route::get('/membru/{id}/licitatii', 'LicitatiiController@licitatiiMembru');
	Route::get('/membru/{id}/licitatii/pagina/{page}', 'LicitatiiController@licitatiiMembru');

	Route::get('/aproba_licitatie/{id}/{value}', function ($id, $value) {
		DB::update('update licitatii set aprobat = ? where id = ?', [$value, $id]);

		return redirect('/licitatie/'. $id);
	});

	Route::get('/aproba_membru/{id}/{value}', function ($id, $value) {
		DB::update('update users set activ = ? where id = ?', [$value, $id]);

		return redirect('/membru/'. $id);
	});

	Route::get('/aproba_membru/{id}/delete', function ($id) {
		DB::delete('delete from users where id = :id', ['id' => $id]);

		return redirect('/');
	});

	Route::post('/licitatie/{id}/adauga_oferta', function (Request $request, $id) {
		$utilizator = DB::select('select * from users where name = :name', ['name' => $request->user()->name]);

		if ($utilizator) {
			$utilizator = $utilizator[0];
			$licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];
            $activa = true;

            // verifica daca licitatia este activa
            if( strtotime($licitatie->data_incepere_licitatie .' '. $licitatie->ora_incepere_licitatie) > time() ||
                strtotime($licitatie->data_expirare_licitatie .' '. $licitatie->ora_expirare_licitatie) < time() ){
                $activa = false;
            }

            // verifica daca utilizatorul este participant
            if ($licitatie->participanti && $activa) { // daca exista participanti SI daca licitatia este activa
                $participanti = json_decode($licitatie->participanti, true);
                if (array_search($utilizator->id, $participanti) !== false) { // note !==<Paste>
                    //verifica daca exista oferte
                    if ($licitatie->oferte) {
                        $oferte = json_decode($licitatie->oferte, true);
                        if (array_search($utilizator->id, array_column($oferte, 'id')) === false) { // note ===
                            array_push($oferte, array(
                                'data' => time(),
                                'id' => $utilizator->id, 
                                'suma' => $request->suma
                                ));
                            DB::update('UPDATE licitatii SET 
                                oferte = ?
                                WHERE id = ?',
                                [
                                    json_encode($oferte),
                                    $id
                                ]);
                        } else {
                            $oferte[array_search($utilizator->id, array_column($oferte, 'id'))]['suma'] = $request->suma;
                            DB::update('UPDATE licitatii SET 
                                oferte = ?
                                WHERE id = ?',
                                [
                                    json_encode($oferte),
                                    $id
                                ]);
                        }
                        return redirect('/licitatie/'.$id);
                    } else { // daca nu exista oferte
                        $oferte = array(0 => array(
                            'data' => time(),
                            'id' => $utilizator->id, 
                            'suma' => $request->suma
                            ));
                        DB::update('UPDATE licitatii SET 
                            oferte = ?
                            WHERE id = ?',
                            [
                                json_encode($oferte),
                                $id
                            ]);
                        return redirect('/licitatie/'.$id);
                    }
                } else {
                    return redirect('/licitatie/'.$id);
                }
            } else {
                return redirect('/licitatie/'.$id);
            }
		} else {
			return redirect('/licitatie/'.$id);
		}
	});

    Route::post('/licitatie/{id}/adauga_participant_solicitare', function (Request $request, $id) 
    { 
		// verifica daca utilizatorul exista 
		$utilizator = DB::select('select * from users where name = :name', ['name' => $request->input('name')]);
		if ($utilizator) {
			$utilizator = $utilizator[0];
			$licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];

			// adauga user la licitatie
			if ($licitatie->participanti_solicitare) { // daca exista alti participanti_solicitare
				// decodifica array-ul si introdu participant now
				$participanti_solicitare = json_decode($licitatie->participanti_solicitare, true);

				// verifica daca utilizatorul este deja participant]
				if (array_search($utilizator->id, $participanti_solicitare) === false) { // note ===
					array_push($participanti_solicitare, $utilizator->id);
					// introdu id utilizator in lista participantilor_solicitare
					DB::update('UPDATE licitatii SET 
			            participanti_solicitare = ?
						WHERE id = ?',
						[
				            json_encode($participanti_solicitare),
				            $id
						]);

                    // incrementeaza numarul de notificari nevizualizate
                    DB::update('update solicitari set solicitari_informatii_num = solicitari_informatii_num + 1 limit 1');
                    // adauga notificare
                    DB::insert('insert into solicitari_informatii (id_user, nume_user, link, actiune) values (?, ?, ?, ?)', [Auth::user()->id, Auth::user()->name, 'licitatie/'.$id, 'participare']);
					return redirect('/licitatie/'.$id);
				} else {				
					return redirect('/licitatie/'.$id);
				}
			} else { // daca nu exsita alti participanti_solicitare
				$participanti_solicitare = array(
						0 => $utilizator->id
					);
				DB::update('UPDATE licitatii SET 
		            participanti_solicitare = ?
					WHERE id = ?',
					[
			            json_encode($participanti_solicitare),
			            $id
					]);
                // incrementeaza numarul de notificari nevizualizate
                DB::update('update solicitari set solicitari_informatii_num = solicitari_informatii_num + 1 limit 1');
                // adauga notificare
                DB::insert('insert into solicitari_informatii (id_user, nume_user, link, actiune) values (?, ?, ?, ?)', [Auth::user()->id, Auth::user()->name, 'licitatie/'.$id, 'participare']);
				return redirect('/licitatie/'.$id);
			}
		} else {
			return redirect('/licitatie/'.$id);
		}
    });

    Route::post('/licitatie/{id}/aproba_participant_solicitare', function (Request $request, $id) 
    { 
		// verifica daca utilizatorul exista 
		$utilizator = DB::select('select * from users where name = :name', ['name' => $request->input('name')]);
		if ($utilizator) {
			$utilizator = $utilizator[0];
			$licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];

            // sterge user din participanti_solicitare
            if ($licitatie->participanti_solicitare) { // daca exista participanti_solicitare
                // decodifica array-ul si sterge participantul
                $participanti_solicitare = json_decode($licitatie->participanti_solicitare, true);

                // verifica daca utilizatorul este deja participant
                if (array_search($utilizator->id, $participanti_solicitare) !== false) { // note !==
                    unset($participanti_solicitare[array_search($utilizator->id, $participanti_solicitare)]);
                    // introdu id utilizator in lista participantilor_solicitare
                    DB::update('UPDATE licitatii SET 
                        participanti_solicitare = ?
                        WHERE id = ?',
                        [
                            json_encode($participanti_solicitare),
                            $id
                        ]);
                }
            }

			// adauga user la licitatie
			if ($licitatie->participanti) { // daca exista alti participanti
				// decodifica array-ul si introdu participant now
				$participanti = json_decode($licitatie->participanti, true);

				// verifica daca utilizatorul este deja participant]
				if (array_search($utilizator->id, $participanti) === false) { // note ===
					array_push($participanti, $utilizator->id);
					// introdu id utilizator in lista participantilor
					DB::update('UPDATE licitatii SET 
			            participanti = ?
						WHERE id = ?',
						[
				            json_encode($participanti),
				            $id
						]);

					// introdu id licitatie in lista licitatiilor la care userul participa
					if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are alte licitatii
						$licitatii_participant = json_decode($utilizator->licitatii_participant, true);
						if (array_search($id, $licitatii_participant) === false) { // verifica daca licitatia este deja in lista utilizatorului
							array_push($licitatii_participant, $id);
							DB::update('UPDATE users SET 
					            licitatii_participant = ?
								WHERE id = ?',
								[
						            json_encode($licitatii_participant),
						            $utilizator->id
								]);
						}
					} else {
						$licitatii_participant = array(0 => $id);				
						DB::update('UPDATE users SET 
				            licitatii_participant = ?
							WHERE id = ?',
							[
					            json_encode($licitatii_participant),
					            $utilizator->id
							]);
					}

					return redirect('/licitatie/'.$id);
				} else {				
					return redirect('/licitatie/'.$id);
				}
			} else { // daca nu exsita alti participanti
				$participanti = array(
						0 => $utilizator->id
					);
				DB::update('UPDATE licitatii SET 
		            participanti = ?
					WHERE id = ?',
					[
			            json_encode($participanti),
			            $id
					]);

					// introdu id licitatie in lista licitatiilor la care userul participa
					if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are alte licitatii
						$licitatii_participant = json_decode($utilizator->licitatii_participant, true);
						if (array_search($id, $licitatii_participant) === false) { // verifica daca licitatia este deja in lista utilizatorului
							array_push($licitatii_participant, $id);
							DB::update('UPDATE users SET 
					            licitatii_participant = ?
								WHERE id = ?',
								[
						            json_encode($licitatii_participant),
						            $utilizator->id
								]);
						}
					} else {
						$licitatii_participant = array(0 => $id);				
						DB::update('UPDATE users SET 
				            licitatii_participant = ?
							WHERE id = ?',
							[
					            json_encode($licitatii_participant),
					            $utilizator->id
							]);
					}

				return redirect('/licitatie/'.$id);
			}
		} else {
			return redirect('/licitatie/'.$id);
		}
    });
    
    Route::post('/licitatie/{id}/sterge_participant_solicitare', function (Request $request, $id) 
    { 
        // verifica daca utilizatorul exista 
        $utilizator = DB::select('select * from users where name = :name', ['name' => $request->input('name')]);
        if ($utilizator) {
            $utilizator = $utilizator[0];
            $licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];
            // sterge user din licitatie
            if ($licitatie->participanti_solicitare) { // daca exista participanti_solicitare
                // decodifica array-ul si sterge participantul
                $participanti_solicitare = json_decode($licitatie->participanti_solicitare, true);

                // verifica daca utilizatorul este deja participant
                if (array_search($utilizator->id, $participanti_solicitare) !== false) { // note !==
                    unset($participanti_solicitare[array_search($utilizator->id, $participanti_solicitare)]);
                    // introdu id utilizator in lista participantilor_solicitare
                    DB::update('UPDATE licitatii SET 
                        participanti_solicitare = ?
                        WHERE id = ?',
                        [
                            json_encode($participanti_solicitare),
                            $id
                        ]);

                    // sterge id licitatie in lista licitatiilor la care userul participa
                    if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are licitatii
                        $licitatii_participant = json_decode($utilizator->licitatii_participant, true);
                        if (array_search($id, $licitatii_participant) !== false) { // verifica daca licitatia este in lista utilizatorului
                            unset($licitatii_participant[array_search($id, $licitatii_participant)]);
                            DB::update('UPDATE users SET 
                                licitatii_participant = ?
                                WHERE id = ?',
                                [
                                    json_encode($licitatii_participant),
                                    $utilizator->id
                                ]);
                        }
                    }

                    return redirect('/licitatie/'.$id);
                } else {
                    return redirect('/licitatie/'.$id);
                }
            } else {
                return redirect('/licitatie/'.$id);
            }
        } else {
            return redirect('/licitatie/'.$id);
        }
    });

	Route::post('/licitatie/{id}/adauga_participant', function (Request $request, $id) {
		// verifica daca utilizatorul exista 
		$utilizator = DB::select('select * from users where name = :name', ['name' => $request->input('name')]);
		if ($utilizator) {
			$utilizator = $utilizator[0];
			$licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];

            // sterge user din lista solicitantilor
            if ($licitatie->participanti_solicitare) { // daca exista participanti_solicitare
                // decodifica array-ul si sterge participantul
                $participanti_solicitare = json_decode($licitatie->participanti_solicitare, true);

                // verifica daca utilizatorul este solicitant
                if (array_search($utilizator->id, $participanti_solicitare) !== false) { // note !==
                    unset($participanti_solicitare[array_search($utilizator->id, $participanti_solicitare)]);
                    // introdu id utilizator in lista participantilor_solicitare
                    DB::update('UPDATE licitatii SET 
                        participanti_solicitare = ?
                        WHERE id = ?',
                        [
                            json_encode($participanti_solicitare),
                            $id
                        ]);
                }
            }

			// adauga user la licitatie
			if ($licitatie->participanti) { // daca exista alti participanti
				// decodifica array-ul si introdu participant now
				$participanti = json_decode($licitatie->participanti, true);

				// verifica daca utilizatorul este deja participant]
				if (array_search($utilizator->id, $participanti) === false) { // note ===
					array_push($participanti, $utilizator->id);
					// introdu id utilizator in lista participantilor
					DB::update('UPDATE licitatii SET 
			            participanti = ?
						WHERE id = ?',
						[
				            json_encode($participanti),
				            $id
						]);

					// introdu id licitatie in lista licitatiilor la care userul participa
					if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are alte licitatii
						$licitatii_participant = json_decode($utilizator->licitatii_participant, true);
						if (array_search($id, $licitatii_participant) === false) { // verifica daca licitatia este deja in lista utilizatorului
							array_push($licitatii_participant, $id);
							DB::update('UPDATE users SET 
					            licitatii_participant = ?
								WHERE id = ?',
								[
						            json_encode($licitatii_participant),
						            $utilizator->id
								]);
						}
					} else {
						$licitatii_participant = array(0 => $id);				
						DB::update('UPDATE users SET 
				            licitatii_participant = ?
							WHERE id = ?',
							[
					            json_encode($licitatii_participant),
					            $utilizator->id
							]);
					}

					return redirect('/licitatie/'.$id);
				} else {				
					return redirect('/licitatie/'.$id);
				}
			} else { // daca nu exsita alti participanti
				$participanti = array(
						0 => $utilizator->id
					);
				DB::update('UPDATE licitatii SET 
		            participanti = ?
					WHERE id = ?',
					[
			            json_encode($participanti),
			            $id
					]);

					// introdu id licitatie in lista licitatiilor la care userul participa
					if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are alte licitatii
						$licitatii_participant = json_decode($utilizator->licitatii_participant, true);
						if (array_search($id, $licitatii_participant) === false) { // verifica daca licitatia este deja in lista utilizatorului
							array_push($licitatii_participant, $id);
							DB::update('UPDATE users SET 
					            licitatii_participant = ?
								WHERE id = ?',
								[
						            json_encode($licitatii_participant),
						            $utilizator->id
								]);
						}
					} else {
						$licitatii_participant = array(0 => $id);				
						DB::update('UPDATE users SET 
				            licitatii_participant = ?
							WHERE id = ?',
							[
					            json_encode($licitatii_participant),
					            $utilizator->id
							]);
					}

				return redirect('/licitatie/'.$id);
			}
		} else {
			return redirect('/licitatie/'.$id);
		}
	});

	Route::post('/licitatie/{id}/sterge_participant', function (Request $request, $id) {
		// verifica daca utilizatorul exista 
		$utilizator = DB::select('select * from users where name = :name', ['name' => $request->input('name')]);
		if ($utilizator) {
			$utilizator = $utilizator[0];
			$licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];
			
			// sterge ofertele userului
			if ($licitatie->oferte) { // daca exista oferte
				$toate_ofertele = json_decode($licitatie->oferte, true);
				$oferte = array_filter($toate_ofertele, function ($k) use ($utilizator) { // use is a closure
					return $k['id'] != $utilizator->id;
				});
				DB::update('UPDATE licitatii SET 
		            oferte = ?
					WHERE id = ?',
					[
			            json_encode($oferte),
			            $id
                    ]);
            }

			// sterge user din licitatie
			if ($licitatie->participanti) { // daca exista participanti
				// decodifica array-ul si sterge participantul
				$participanti = json_decode($licitatie->participanti, true);

				// verifica daca utilizatorul este deja participant
				if (array_search($utilizator->id, $participanti) !== false) { // note !==
					unset($participanti[array_search($utilizator->id, $participanti)]);
					// introdu id utilizator in lista participantilor
					DB::update('UPDATE licitatii SET 
			            participanti = ?
						WHERE id = ?',
						[
				            json_encode($participanti),
				            $id
						]);

					// sterge id licitatie in lista licitatiilor la care userul participa
					if ($utilizator->licitatii_participant) { // verifica daca utilizatorul are licitatii
						$licitatii_participant = json_decode($utilizator->licitatii_participant, true);
						if (array_search($id, $licitatii_participant) !== false) { // verifica daca licitatia este in lista utilizatorului
							unset($licitatii_participant[array_search($id, $licitatii_participant)]);
							DB::update('UPDATE users SET 
					            licitatii_participant = ?
								WHERE id = ?',
								[
						            json_encode($licitatii_participant),
						            $utilizator->id
								]);
						}
					}

					return redirect('/licitatie/'.$id);
				} else {
					return redirect('/licitatie/'.$id);
				}
			} else {
				return redirect('/licitatie/'.$id);
			}
		} else {
			return redirect('/licitatie/'.$id);
		}
	});

	Route::post('/licitatie/{id}/sterge_oferta', function (Request $request, $id) {
		$licitatie = DB::select('select * from licitatii where id = :id', ['id' => $id])[0];
		$toate_ofertele = json_decode($licitatie->oferte, true);
		$oferte = array_filter($toate_ofertele, function ($k) use ($request) { // use is a closure
			return ( $k['id'] != $request->input('id') || ( $k['id'] == $request->input('id') && $k['data'] != $request->input('data') ) );
		});
		DB::update('UPDATE licitatii SET 
            oferte = ?
			WHERE id = ?',
			[
	            json_encode($oferte),
	            $id
			]);
		return redirect('/licitatie/'.$id);
	});

	Route::get('/licitatie/{id}/admin-oferte', function (Request $request, $id) {
		$existing = DB::select('select * from licitatii where id = :id', ['id' => $id]);
		$oferte = '';

        // daca exista oferte, stochaza datele acestora intr-un array
		if ($existing[0]->oferte) {
			$oferte = json_decode($existing[0]->oferte, true);
			foreach ($oferte as $key => $value) {
				$oferte[$key]['nume'] = DB::select('select name from users where id = :id', ['id' => $value['id']])[0]->name;
			}
		}

		return view('admin-oferte')->with('licitatie', $existing[0])->with(['oferte' => $oferte]);
	});
	
	Route::get('/licitatie/{id}/participant-oferte', function (Request $request, $id) {
		$existing = DB::select('select * from licitatii where id = :id', ['id' => $id]);
		$oferte = '';

        // daca exista oferte, stochaza datele acestora intr-un array
		if ($existing[0]->oferte) {
			$oferte = json_decode($existing[0]->oferte, true);
			foreach ($oferte as $key => $value) {
				$oferte[$key]['nume'] = DB::select('select name from users where id = :id', ['id' => $value['id']])[0]->name;
			}
		}

		return view('participant-oferte')->with(['oferte' => $oferte]);
	});

    Route::get('/solicitari_informatii', function (Request $request) {
        // reseteaza numarul de notificari nevizualizate
        DB::update('update solicitari set solicitari_informatii_num = 0 limit 1');
        $solicitari = DB::select('select * from solicitari_informatii order by id desc');
        return view('solicitariInformatii')->with([
            'solicitari' => $solicitari
        ]);
    });

    Route::get('/sterge_solicitare/{id}', function (Request $request, $id) {
        DB::delete('delete from solicitari_informatii where id = :id', ['id' => $id]);
        return redirect('/solicitari_informatii');
    });
});
