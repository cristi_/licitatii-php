$("#admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}").submit(function(e) {
    var url = "{{url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta"; // the script where you handle the form input.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}").serialize(), // serializes the form's elements.
        success: function(data)
        {
            try {
                socket.send('update');
            } catch (e) {
                console.log(e);
            }
        $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/admin-oferte");
        }
    });
    e.preventDefault(); // avoid to execute the actual submit of the form.
});
