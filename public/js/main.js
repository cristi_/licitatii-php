/*
 * Timpul pentru ecranul licitatiilor live
 */
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('time').innerHTML =
        h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

/* 
 * Pentru meniual din dreapta
 */
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

/*
 * Pentru inputurile de ales data calendaristica
 */
$(document).ready(function() {
    $.fn.datepicker.defaults.language = 'ro';
    $('.datepicker-top').datepicker({
        format: "yyyy-mm-dd",
        language: "ro",
        todayBtn: "linked",
        orientation: "top auto",
        todayHighlight: true
    });
    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "ro",
        todayBtn: "linked",
        orientation: "bottom auto",
        todayHighlight: true
    });
});

/*
 * Pentru popupul de a introduce oferta
 */
(function($) {
    $.fn.drags = function(opt) {
        opt = $.extend({handle:"",cursor:"move"}, opt);
        if(opt.handle === "") {
            var $el = this.find('.titlebar');
        } else {
            var $el = this.find('.titlebar').find(opt.handle);
        }
        return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
            if(opt.handle === "") {
                var $drag = $(this).addClass('draggable');
            } else {
                var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
            }
            var z_idx = $drag.css('z-index'),
            drg_h = $drag.outerHeight(),
            drg_w = $drag.outerWidth(),
            pos_y = $drag.offset().top + drg_h - e.pageY,
            pos_x = $drag.offset().left + drg_w - e.pageX;
            $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
                $('.draggable').parent().offset({
                    top:e.pageY + pos_y - drg_h,
                    left:e.pageX + pos_x - drg_w
                }).on("mouseup", function() {
                    $(this).removeClass('draggable').css('z-index', z_idx);
                });
            });
            e.preventDefault(); // disable selection
        }).on("mouseup", function() {
            if(opt.handle === "") {
                $(this).removeClass('draggable');
            } else {
                $(this).removeClass('active-handle').parent().removeClass('draggable');
            }
        });
    }
})(jQuery);
var camp_adauga_oferta = $("#camp-adauga-oferta");
camp_adauga_oferta.drags();
$('#adauga_oferta_popover_btn').click(function () {
    $('#camp-adauga-oferta').toggle().css({'top':'0', 'left':'0'});
    document.getElementById('oferta').focus();
});
$('#oferta-popover-close').click(function () {
    camp_adauga_oferta.hide().css({'top':'0', 'left':'0'});
})

/*
 * Pentru butoanele de ascunere a sectiunilor
 */
$(document).ready(function(){
    // prezentare scurta
    $('.sectiune-ascunde').find('.ascunde').click(function () {
        $(this).closest('.sectiune-ascunde').hide();
        $('.prezentare-ascuns').toggle();
        $('.prezentare-vizibil').toggle();
    });
    $('#toggle-prezentare').click(function () {
        $('#sectiune-prezentare').toggle();
        $('.prezentare-ascuns').toggle();
        $('.prezentare-vizibil').toggle();
    });
    // lista licitatii
    $('#sectiune-lista-licitatii').find('.ascunde').click(function () {
        $('#sectiune-lista-licitatii').toggle();
        $('.lista-licitatii-ascuns').toggle();
        $('.lista-licitatii-vizibil').toggle();
        $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-9').removeClass('col-md-9').addClass('col-md-12');
        $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-12 > div.panel.panel-default.right-corners-right').removeClass('right-corners-right').addClass('right-corners-both');
        $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-12 > div.row').css({'margin':'0 10px'});
    });
    $('#toggle-lista-licitatii').click(function () {
        $('#sectiune-lista-licitatii').toggle();
        $('.lista-licitatii-ascuns').toggle();
        $('.lista-licitatii-vizibil').toggle();
        if ($('#page-content-wrapper > div > div > div > div > div.row > div.col-md-9').hasClass('col-md-9')) {
            $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-9').removeClass('col-md-9').addClass('col-md-12');
            $('#sectiune-prezentare').removeClass('right-corners-right').addClass('right-corners-both');
            $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-12 > div.row').css({'margin':'0 10px'});
        } else {
            console.log('aici');
            $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-12').removeClass('col-md-12').addClass('col-md-9');
            $('#sectiune-prezentare').removeClass('right-corners-both').addClass('right-corners-right');
            $('#page-content-wrapper > div > div > div > div > div.row > div.col-md-9 > div.row').css({'margin':'0 -15px'});
        }
    });
    // chat
});

/*
 * Pentru butonul de a adauga oferta
 */
$('#oferta').on('change keyup', function(event) {
    if ( event.target.validity.valid ) {
        $('#error').hide();
        $('#buton-oferta').addClass('btn-default');
        $('#buton-oferta').removeClass('btn-danger');
        $('#buton-oferta').prop('disabled', false);
    } else {
        $('#buton-oferta').prop('disabled', true);
        $('#buton-oferta').removeClass('btn-default');
        $('#buton-oferta').addClass('btn-danger');
        $('#error').show();
    }    
});
$("#participant-oferte").submit(function(e) {
    camp_adauga_oferta.hide().css({'top':'0', 'left':'0'});
    if ($('#oferta').val().length > 0){
        var url = $('#oferta').data('url'); // the script where you handle the form input.
        $.ajax({
            type: "POST",
            url: url,
            data: $("#participant-oferte").serialize(), // serializes the form's elements.
            success: function(data)
            {
                try {
                    socket.send('update');
                } catch (e) {
                    console.log(e);
                }
                $('#oferte').load( $(this).data('url') );
            }
        });
    }
    e.preventDefault(); // avoid to execute the actual submit of the form.
    document.getElementById('participant-oferte').reset();
});

/*
 * Pentru dropdownuri -- le interzice sa mearga offscreen
 */
$(document).on('shown.bs.dropdown', function () {
    OffsetDropdown();
    $(window).on('resize.bs.dropdown', function () {
        OffsetDropdown();
    })
})
$(document).on('hide.bs.dropdown', function() {
    $(window).off('resize.bs.dropdown');
})
var OffsetDropdown = function() {
    var dropdown = $('.open>.dropdown-menu');
    if (dropdown.length == 0) {
        return;
    }
    var rightOffset = dropdown.offset().left + dropdown.width();
    var browserWidth = $('body').innerWidth();
    var neededLeftOffset = dropdown.position().left - (rightOffset - browserWidth);
    if (neededLeftOffset < 0) {
        dropdown.css({ left: neededLeftOffset - 3 });
    } else {
        dropdown.css({ left: 0 });
    }
}
