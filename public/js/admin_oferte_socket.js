var host   = 'ws://176.223.125.107:8891';
var socket = null;

try {
        socket = new WebSocket(host);

    //Manages the open event within your client code
    socket.onopen = function () {
        console.log('Connection Opened');
        return;
    };

    //Manages the message event within your client code
    socket.onmessage = function (msg) {
        console.log(msg.data);
        $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/admin-oferte");
        return;

    };

    //Manages the close event within your client code
    socket.onclose = function () {
        console.log('Connection Closed');
        return;
    };
} catch (e) {
    console.log(e);
}

