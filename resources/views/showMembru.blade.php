@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Informatii personale ale membrului {{ $membru->name }}
          <div class="pull-right">
            @if (Auth::user()->admin)
                @if ($membru->initiator)
                    <a href="{{ url('/membru') }}/{{ $membru->id }}/licitatii" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-list"></i> licitatii initiate</a>
                @else
                    <a href="{{ url('/membru') }}/{{ $membru->id }}/licitatii" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-list"></i> licitatii la care participa</a>
                @endif
                <a href="{{ url('/membru') }}/{{ $membru->id }}/edit" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile</a>
                @if ($membru->activ)
                    <a href="{{ url('/aproba_membru') }}/{{$membru->id}}/0" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-remove"></i> dezactiveaza</a>
                @else
                    <a href="{{ url('/aproba_membru') }}/{{$membru->id}}/1" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-ok"></i> activeaza</a>
                @endif
            @else
            @if ($membru->initiator)
                <a href="{{ url('/membru') }}/{{ $membru->id }}/licitatii" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-list"></i> licitatii initiate</a>
            @else
                <a href="{{ url('/membru') }}/{{ $membru->id }}/licitatii" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-list"></i> licitatii la care participi</a>
            @endif
            <a href="{{ url('/membru') }}/{{ $membru->id }}/edit" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile</a>
            @endif
          </div>
          <div class="clearfix"></div>
        </div>
        <table class="table">
          <tbody>
@if (Auth::user()->admin)
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Nume</strong></td>
              <td class="col-sm-9">{{ $membru->name }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Administrator</strong></td>
              <td class="col-sm-9">{{ $membru->admin ? 'da' : 'nu' }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Initiator</strong></td>
              <td class="col-sm-9">{{ $membru->initiator ? 'da' : 'nu' }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Activ</strong></td>
              <td class="col-sm-9">{{ $membru->activ ? 'da' : 'nu' }}</td>
            </tr>
@endif
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Email</strong></td>
              <td class="col-sm-9">{{ $membru->email }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Adresa</strong></td>
              <td class="col-sm-9">{{ $membru->adresa }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">CUI</strong></td>
              <td class="col-sm-9">{{ $membru->cui }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Persoana de contact</strong></td>
              <td class="col-sm-9">{{ $membru->contact }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Telefon</strong></td>
              <td class="col-sm-9">{{ $membru->telefon }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Alte informatii</strong></td>
              <td class="col-sm-9">{{ $membru->alte }}</td>
            </tr>
          </tbody>
        </table>
<br>

@if ($membru->initiator && $detalii_initiator)
        <div class="panel-heading" style="border-top:1px solid #DDDDDD; border-radius: 0;">Informatii de initiator ale membrului {{ $membru->name }}
          <div class="pull-right">
            <a href="{{ url('/membru') }}/{{ $membru->id }}/editInitiator" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile de initiator</a>
          </div>
          <div class="clearfix"></div>
        </div>
        <table class="table">
          <tbody>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Cantitate estimata in MWh</strong></td>
              <td class="col-sm-9">{{ $detalii_initiator->cantitate }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Nivel de tensiune</strong></td>
              <td class="col-sm-9">{{ $detalii_initiator->nivel }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Perioada de livrare</strong></td>
              <td class="col-sm-9">Intre {{ date("d M Y", strtotime(str_replace('-', '/', $detalii_initiator->perioada_start))) }} si {{ date("d M Y", strtotime(str_replace('-', '/', $detalii_initiator->perioada_sfarsit))) }}
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Data inceperi livrarii</strong></td>
              <td class="col-sm-9">{{ date("d M Y", strtotime(str_replace('-', '/', $detalii_initiator->incepere))) }}
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Termen de plata dorit</strong></td>
              <td class="col-sm-9">{{ $detalii_initiator->termen }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Modalitate de plata dorita</strong></td>
              <td class="col-sm-9">{{ $detalii_initiator->modalitate }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Program de lucru</strong></td>
              <td class="col-sm-9">{{ $detalii_initiator->program }}</td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Lista locuri de consum</strong></td>
              <td class="col-sm-9"><a class="btn btn-default btn-sm" href="{{ url($detalii_initiator->consum_locuri) }}" download>Descarca fisier</a></td>
            </tr>
            <tr>
              <td class="col-sm-3"><strong class="pull-right">Consumuri anuale estimate</strong></td>
              <td class="col-sm-9"><a class="btn btn-default btn-sm" href="{{ url($detalii_initiator->consum_anual) }}" download>Descarca fisier</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
@endif
@endsection
