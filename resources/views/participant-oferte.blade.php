<h3>Oferte:</h3>
@if ($oferte != '')
@foreach ($oferte as $oferta)
      @if ($oferta['nume'] == Auth::user()->name)
      <mark>
      ({{ date("h:i:s",$oferta['data']) }}) tu oferit suma: <strong>{{ $oferta['suma'] }}</strong></a>
    </mark>
      @else
      ({{ date("h:i:s",$oferta['data']) }}) Furnizor #{{ hash('crc32b', $oferta['nume']) }} a oferit suma: <strong>{{ $oferta['suma'] }}</strong></a>
      @endif
<br>
@endforeach
@else
<p>nu exista oferte</p>
@endif
<hr>
