<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Enerlic</title>

  <!-- Fonts -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

  <!-- Styles -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
  <link href="{{ url('css/main.css') }}" rel="stylesheet">

  <!-- JavaScripts -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

  <!-- include summernote css/js-->
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.js"></script>

  <!-- include datepicker -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.standalone.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/locales/bootstrap-datepicker.ro.min.js"></script>

</head>
<body id="app-layout" onload="startTime()">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">

        <!-- Collapsed Hamburger -->
        <a href="#menu-toggle" type="button" class="btn btn-default navbar-brand" id="menu-toggle" style="margin-right:20px;">
          <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
        </a>
        <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/') }}">
          Enerlic
        </a>
      </div>

      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav pull-left">
        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
        <li>
            @if (Auth::user())
          <a href="{{ url('/membru').'/'.Auth::user()->id }}">
            <i class="fa fa-btn fa-user"></i>{{ Auth::user()->name }}
          </a>
            @else
        <li><a href="{{ url('/login') }}"><i class="fa fa-btn fa-sign-in"></i>Vizitator</a></li>
            @endif
        </li>
        </ul>
      </div>
    </div>
  </nav>


  <div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav">
        @if (Auth::user())
        <li class="sidebar-brand">
          <a href="{{ url('/membru').'/'.Auth::user()->id }}">
            <i class="fa fa-btn fa-user"></i>Contul meu 
          </a>
        </li>
        @endif
        <li><a href="{{ url('/licitatii') }}"><i class="fa fa-btn fa-list"></i>Lista licitatii</a></li>
        @if (Auth::user())
        @if (Auth::user()->admin)
<?php
$solicitari = DB::select('select * from solicitari limit 1')[0];
?>
        <li><a href="{{ url('/membrii') }}"><i class="fa fa-btn fa-users"></i>Lista utilizatori</a></li>
        <li><a href="{{ url('/licitatie_noua') }}"><i class="fa fa-btn fa-plus"></i>Adauga licitatie</a></li>
        <li><a href="{{ url('/register') }}"><i class="fa fa-btn fa-user-plus"></i>Adauga utilizator</a></li>
        <li><a href="{{ url('/solicitari') }}"><i class="fa fa-btn fa-list"></i>Solicitari licitatiii <span class="badge">{{ $solicitari->solicitari_licitatii_num }}</span></a></li>
        <li><a href="{{ url('/solicitari_informatii') }}"><i class="fa fa-btn fa-list"></i>Solicitari informatii <span class="badge">{{ $solicitari->solicitari_informatii_num }}</span></a></li>
        @elseif (Auth::user()->initiator)
        <li><a href="{{ url('/licitatie_noua') }}"><i class="fa fa-btn fa-plus"></i>Solicita licitatie</a></li>
        @else
        @endif
        @if (!Auth::user()->admin)
        <li><a href="{{ url('/membru/'.Auth::user()->id.'/licitatii') }}"><i class="fa fa-btn fa-list"></i>Licitatiile mele</a></li>
        @endif
        @endif
        @if (Auth::guest())
        <li><a href="{{ url('/login') }}"><i class="fa fa-btn fa-sign-in"></i>Login</a></li>
        @else
        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
        @endif
      </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="row">
          @yield('content')
        </div>
      </div>
    </div>
  </div>


<script src="/js/main.js"></script>
</body>
</html>
