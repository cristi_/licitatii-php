@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Lista membrii
          <div class="pull-right">
            <a href="{{ url('/membrii/initiatori') }}" class="btn btn-default btn-sm"> vezi initiatori</a>
            <a href="{{ url('/membrii/furnizori') }}" class="btn btn-default btn-sm"> vezi furnizori</a>
          </div>
          <div class="clearfix"></div>
        </div>
        <table class="table table-hover table-striped panel-body">
          <thead>
            <tr>
              <th>#</th>
              <th>Denumire</th>
              <th>Tip Utilizator</th>
              <th>Email</th>
              <th>Data inregistrarii</th>
              <th>Actiuni</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($membrii as $user)
            <tr>
              <td>{{$user->id}}</td>
              <td><a href="{{url('/membru/').'/'.$user->id}}">{{$user->name}}</a></td>
              @if ($user->admin)
              <td>Administrator</td>
              @else
              @if ($user->initiator)
              <td>Initiator</td>
              @else
              <td>Furnizor</td>
              @endif
              @endif
              <td>{{$user->email}}</td>
          <td>{{ date("d M Y, H:i:s", strtotime(str_replace('-', '/', $user->created_at))) }}</td>
              @if ($user->admin)
              <td></td>
              @else
              <td>
                @if ($user->activ)
                <a href="{{ url('/aproba_membru') }}/{{$user->id}}/0" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i> dezactiveaza</a>
                @else
                <a href="{{ url('/aproba_membru') }}/{{$user->id}}/1" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-ok"></i> activeaza</a>
                @endif
              </td>
@endif
            </tr>
            @endforeach
          </tbody>
        </table>
        <div class="panel-footer">
          <nav class="text-center">
            <ul class="pagination" style="margin:0;">
              @if (isset($opt))
              @if(0 > $page-1)
              <li class="previous disabled"><a><span aria-hidden="true">&larr;</span> Inapoi</a></li>
              @elseif(0 > $page-2)
              <li class="previous"><a href="{{ url('membrii/').'/'.$opt.'/pagina/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
              @for ($i = $page - 1; $page > $i; $i++)
              <li><a href="{{ url('membrii').'/'.$opt.'/pagina/'.$i }}}">{{$i}}</a></li>
              @endfor
              @else
              <li class="previous"><a href="{{ url('membrii').'/'.$opt.'/pagina/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
              @for ($i = $page - 2; $page > $i; $i++)
              <li><a href="{{ url('membrii').'/'.$opt.'/pagina/'.$i }}">{{$i}}</a></li>
              @endfor
              @endif
              <li class="active"><a href="{{ url('membrii').'/'.$opt.'/pagina/'.$page }}">{{$page}}</a></li>
              @for ($i = $page + 1; $page + 3 > $i; $i++)
              <li><a href="{{ url('membrii').'/'.$opt.'/pagina/'.$i }}">{{$i}}</a></li>
              @endfor
              <li class="next"><a href="{{ url('membrii').'/'.$opt.'/pagina/'.($page+1) }}">Inainte <span aria-hidden="true">&rarr;</span></a></li>

              @else
              @if(0 > $page-1)
              <li class="previous disabled"><a><span aria-hidden="true">&larr;</span> Inapoi</a></li>
              @elseif(0 > $page-2)
              <li class="previous"><a href="{{ url('membrii/pagina/').'/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
              @for ($i = $page - 1; $page > $i; $i++)
              <li><a href="{{ url('membrii/pagina/').'/'.$i }}}">{{$i}}</a></li>
              @endfor
              @else
              <li class="previous"><a href="{{ url('membrii/pagina/').'/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
              @for ($i = $page - 2; $page > $i; $i++)
              <li><a href="{{ url('membrii/pagina/').'/'.$i }}">{{$i}}</a></li>
              @endfor
              @endif
              <li class="active"><a href="{{ url('membrii/pagina/').'/'.$page }}">{{$page}}</a></li>
              @for ($i = $page + 1; $page + 3 > $i; $i++)
              <li><a href="{{ url('membrii/pagina/').'/'.$i }}">{{$i}}</a></li>
              @endfor
              <li class="next"><a href="{{ url('membrii/pagina/').'/'.($page+1) }}">Inainte <span aria-hidden="true">&rarr;</span></a></li>
              @endif
            </ul>
          </nav>
        </div>
      </div>
    </div>
@endsection
