@extends('layouts.app')

@section('content')
<div class="panel panel-default">
  <div class="panel-heading">Licitatii
@if (isset($location))
<?php 
    preg_match_all('!\d+!', $location, $matches); 
    if ($matches[0]) { $id = $matches[0][0]; } 
    else { $id = 0; }
    if ($id == Auth::user()->id) { $tip_buton = 'contul meu'; }
    else { $tip_buton = 'pagina membrului'; }
?>
    @if (isset($tip_membru) && $tip_membru == 'initiator')
      {{ 'initiate de ' . $membru }}
        <div class="pull-right">
            <a href="{{ url('/membru') }}/{{ $id }}" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-user"></i> {{ $tip_buton }}</a>
        </div>
        <div class="clearfix"></div>
    @elseif (isset($tip_membru) && $tip_membru == 'furnizor')
      {{ 'la care ' . $membru . ' participa' }}
        <div class="pull-right">
            <a href="{{ url('/membru') }}/{{ $id }}" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-user"></i> {{ $tip_buton }}</a>
        </div>
        <div class="clearfix"></div>
    @else
      {{ $location == 'solicitari' ? 'solicitate' : $location }}
    @endif
@else
    @if (isset($active))
    {{ $active == '0' ? '' : $active }}
    @endif
    @if (isset($type))
    {{ $type == '0' ? '' : $type }}
    @endif
    <div class="pull-right">
      @if (isset($type) || isset($active))
      @if (isset($type) && isset($active))
      @if ($type == 'live' && $active == 'active')
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/live/status/0/pagina/0') }}" type="button" class="btn btn-info active">Active</a>
        <a href="{{ url('licitatii/tip/live/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Inactive</a>
      </div>
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/0/status/active/pagina/0') }}" type="button" class="btn btn-info active">Live</a>
        <a href="{{ url('licitatii/tip/termen/status/active/pagina/0') }}" type="button" class="btn btn-default">Termen</a>
      </div>
      @elseif ($type == 'live' && $active == 'inactive')
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/live/status/active/pagina/0') }}" type="button" class="btn btn-default">Active</a>
        <a href="{{ url('licitatii/tip/live/status/0/pagina/0') }}" type="button" class="btn btn-info active">Inactive</a>
      </div>
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/0/status/inactive/pagina/0') }}" type="button" class="btn btn-info active">Live</a>
        <a href="{{ url('licitatii/tip/termen/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Termen</a>
      </div>
      @elseif ($type == 'termen' && $active == 'active')
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/termen/status/0/pagina/0') }}" type="button" class="btn btn-info active">Active</a>
        <a href="{{ url('licitatii/tip/termen/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Inactive</a>
      </div>
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/live/status/active/pagina/0') }}" type="button" class="btn btn-default">Live</a>
        <a href="{{ url('licitatii/tip/0/status/active/pagina/0') }}" type="button" class="btn btn-info active">Termen</a>
      </div>
      @elseif ($type == 'termen' && $active == 'inactive')
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/termen/status/active/pagina/0') }}" type="button" class="btn btn-default">Active</a>
        <a href="{{ url('licitatii/tip/termen/status/0/pagina/0') }}" type="button" class="btn btn-info active">Inactive</a>
      </div>
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/live/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Live</a>
        <a href="{{ url('licitatii/tip/0/status/inactive/pagina/0') }}" type="button" class="btn btn-info active">Termen</a>
      </div>
      @else
      @if (isset($active))
          @if ($active == 'active')
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/') }}" type="button" class="btn btn-info active">Active</a>
            <a href="{{ url('licitatii/tip/0/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Inactive</a>
          </div>
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/tip/live/status/active/pagina/0') }}" type="button" class="btn btn-default">Live</a>
            <a href="{{ url('licitatii/tip/termen/status/active/pagina/0') }}" type="button" class="btn btn-default">Termen</a>
          </div>
          @elseif ($active == 'inactive')
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/tip/0/status/active/pagina/0') }}" type="button" class="btn btn-default">Active</a>
            <a href="{{ url('licitatii/') }}" type="button" class="btn btn-info active">Inactive</a>
          </div>
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/tip/live/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Live</a>
            <a href="{{ url('licitatii/tip/termen/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Termen</a>
          </div>
          @endif
      @endif
      @if (isset($type))
          @if ($type == 'termen')
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/tip/termen/status/active/pagina/0') }}" type="button" class="btn btn-default">Active</a>
            <a href="{{ url('licitatii/tip/termen/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Inactive</a>
          </div>
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/tip/live/status/0/pagina/0') }}" type="button" class="btn btn-default">Live</a>
            <a href="{{ url('licitatii/') }}" type="button" class="btn btn-info active">Termen</a>
          </div>
          @elseif ($type == 'live')
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/tip/live/status/active/pagina/0') }}" type="button" class="btn btn-default">Active</a>
            <a href="{{ url('licitatii/tip/live/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Inactive</a>
          </div>
          <div class="btn-group" role="group" aria-label="...">
            <a href="{{ url('licitatii/') }}" type="button" class="btn btn-info active">Live</a>
            <a href="{{ url('licitatii/tip/termen/status/0/pagina/0') }}" type="button" class="btn btn-default">Termen</a>
          </div>
          @endif
      @endif
      @endif
      @endif
      @elseif (!isset($categorie))
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/0/status/active/pagina/0') }}" type="button" class="btn btn-default">Active</a>
        <a href="{{ url('licitatii/tip/0/status/inactive/pagina/0') }}" type="button" class="btn btn-default">Inactive</a>
      </div>
      <div class="btn-group" role="group" aria-label="...">
        <a href="{{ url('licitatii/tip/live/status/0/pagina/0') }}" type="button" class="btn btn-default">Live</a>
        <a href="{{ url('licitatii/tip/termen/status/0/pagina/0') }}" type="button" class="btn btn-default">Termen</a>
      </div>
      @endif
    </div>
    <div class="clearfix"></div>
@endif
  </div>
  <table class="table table-hover panel-body">
    <thead>
      <tr>
        <th>#</th>
        <th>Denumire licitatie</th>
        <th>Denumire initiator</th>
        <th>Status</th>
        <th>Tip licitatie</th>
        <th>Incepere licitatie</th>
        @if (Auth::user() && Auth::user()->admin)
        <th>Actiuni</th>
        @endif
      </tr>
    </thead>
    <tbody>
@if (isset($licitatii))
      @forelse ($licitatii as $licitatie)
      @if (strtotime($licitatie->data_incepere_licitatie .' '. $licitatie->ora_incepere_licitatie) < time() && strtotime($licitatie->data_expirare_licitatie .' '. $licitatie->ora_expirare_licitatie) > time())
      <tr class="">
        @else
        <tr class="warning">
          @endif
          <td>{{$licitatie->id}}</td>
          <td><a href="{{ url('/licitatie') }}/{{$licitatie->id}}">{{$licitatie->denumire_licitatie}}</a></td>
          @if (Auth::user()->admin)
          <td><a href="{{ url('/membru') }}/{{$licitatie->id_initiator}}">{{$licitatie->denumire_initiator}}</a></td>
          @else
          <td>{{$licitatie->denumire_initiator}}</td>
          @endif
          @if (strtotime($licitatie->data_incepere_licitatie .' '. $licitatie->ora_incepere_licitatie) < time() && strtotime($licitatie->data_expirare_licitatie .' '. $licitatie->ora_expirare_licitatie) > time())
          <td>activa</td>
          @else
          <td>inactiva</td>
          @endif
          @if ($licitatie->live)
          <td>live</td>
          @else
          <td>termen</td>
          @endif
          <td>{{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->data_incepere_licitatie))) }}, {{ $licitatie->ora_incepere_licitatie }}</td>
          @if (Auth::user()->admin)
          <td>
            @if ($licitatie->aprobat)
            <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/0" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i> dezaproba</a>
            @else
            <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/1" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-ok"></i> aproba</a>
            @endif
            <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/delete" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> sterge licitatia</a>
          </td>
          @endif
        </tr>
        @empty
        <tr>
        </tr>
        @endforelse
        @else
        <tr>
        </tr>
@endif
      </tbody>
    </table>
    <div class="panel-footer">
      <nav class="text-center">
        <ul class="pagination" style="margin:0;">
          @if (isset($type) && isset($active))

          @if(0 > $page-1)
          <li class="previous disabled"><a><span aria-hidden="true">&larr;</span> Inapoi</a></li>
          @elseif(0 > $page-2)
          <li class="previous"><a href="{{ url(isset($location) ? $location : 'licitatii/' ). '/tip/'.$type . '/status/'.$active .'/pagina/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
          @for ($i = $page - 1; $page > $i; $i++)
          <li><a href="{{ url(isset($location) ? $location : 'licitatii/'). '/tip/'.$type . '/status/'.$active .'/pagina/'.$i }}">{{$i}}</a></li>
          @endfor
          @else
          <li class="previous"><a href="{{ url(isset($location) ? $location : 'licitatii/'). '/tip/'.$type . '/status/'.$active .'/pagina/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
          @for ($i = $page - 2; $page > $i; $i++)
          <li><a href="{{ url(isset($location) ? $location : 'licitatii/'). '/tip/'.$type . '/status/'.$active .'/pagina/'.$i }}">{{$i}}</a></li>
          @endfor
          @endif
          <li class="active"><a href="{{ url(isset($location) ? $location : 'licitatii/'). '/tip/'.$type . '/status/'.$active .'/pagina/'.$page }}">{{$page}}</a></li>
          @for ($i = $page + 1; $page + 3 > $i; $i++)
          <li><a href="{{ url(isset($location) ? $location : 'licitatii/'). '/tip/'.$type . '/status/'.$active .'/pagina/'.$i }}">{{$i}}</a></li>
          @endfor
          <li class="next"><a href="{{ url(isset($location) ? $location : 'licitatii/'). '/tip/'.$type . '/status/'.$active .'/pagina/'.($page+1) }}">Inainte <span aria-hidden="true">&rarr;</span></a></li>

          @elseif (!isset($categorie))

          @if(0 > $page-1)
          <li class="previous disabled"><a><span aria-hidden="true">&larr;</span> Inapoi</a></li>
          @elseif(0 > $page-2)
          <li class="previous"><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
          @for ($i = $page - 1; $page > $i; $i++)
          <li><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.$i }}">{{$i}}</a></li>
          @endfor
          @else
          <li class="previous"><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.($page-1) }}"><span aria-hidden="true">&larr;</span> Inapoi</a></li>
          @for ($i = $page - 2; $page > $i; $i++)
          <li><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.$i }}">{{$i}}</a></li>
          @endfor
          @endif
          <li class="active"><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.$page }}">{{$page}}</a></li>
          @for ($i = $page + 1; $page + 3 > $i; $i++)
          <li><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.$i }}">{{$i}}</a></li>
          @endfor
          <li class="next"><a href="{{ url(isset($location) ? $location : 'licitatii').'/pagina/'.($page+1) }}">Inainte <span aria-hidden="true">&rarr;</span></a></li>

          @endif
        </ul>
      </nav>
    </div>
  </div>
  @endsection
