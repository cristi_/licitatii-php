@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Introducere licitatie in sistem</div>
        <div class="panel-body">
          <form role="form" enctype="multipart/form-data" method="POST" action="{{ url('/licitatie_noua') }}">
            {!! csrf_field() !!}
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Licitatie live </label>
              <div class="col-sm-9" id="contains-checkbox">
                {!! Form::hidden('live', false) !!}
                {!! Form::checkbox('live', true) !!}
              </div>
            </div>
            <div class="show_if_live form-group row">
              <label class="col-sm-3 form-control-label" for="">Durata etapelor in minute</label>
              <div class="col-sm-9">
                <div class="input-daterange input-group">
                    <span class="input-group-addon">prima etapa:</span>
                    <input type="number" class="form-control" name="durata_etapa_1" value="10">
                    <span class="input-group-addon" style="border-left: none; border-right: none;">a doua etapa:</span>
                    <input type="number" class="form-control" name="durata_etapa_2" value="10">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="denumire_initiator" class="col-sm-3 form-control-label">Initiator</label>
              <div class="col-sm-9">
                @if (Auth::user()->admin)
                @if ($utilizatori)
                  <select name="denumire_initiator" class="form-control">
                    @foreach ($utilizatori as $utilizator)
                    <option value="{{$utilizator->name}}">{{$utilizator->name}}</option>
                    @endforeach
                  </select>
                @else
                    <p><span class="text-danger"><strong>Nu exista initiatori!</strong></span> Trebuie sa existe initiatori in baza de date pentru a adauga o licitatie.</p>
                @endif
                @else
                <input readonly required type="text" class="form-control" name="denumire_initiator" id="denumire_initiator" value="{{ Auth::user()->name }}">
                @endif
              </div>
            </div>
@if (Auth::user()->admin)
            <div class="form-group row">
              <label for="denumire_licitatie" class="col-sm-3 form-control-label">Denumire licitatie</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="denumire_licitatie" id="denumire_licitatie" placeholder="Denumire licitatie">
              </div>
            </div>
@endif
            <div class="form-group row">
              <label for="cantitate" class="col-sm-3 form-control-label">Cantitate estimata in MWh</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="cantitate" id="cantitate" placeholder="Cantitate estimata in MWh">
              </div>
            </div>
            <div class="form-group row">
              <label for="tensiune" class="col-sm-3 form-control-label">Nivel de tensiune</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="tensiune" id="tensiune" placeholder="Nivel de tensiune">
              </div>
              </div>


<div class="form-group row">
    <label class="col-sm-3 form-control-label">Perioada de livrare</label>
    <div class="col-sm-9">
        <div class="input-daterange input-group">
            <span class="input-group-addon">intre</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')" required data-provide="datepicker" class="form-control datepicker" name="perioada_start" id="perioada_start" placeholder="Inceperea livrarii">
            <span class="input-group-addon" style="border-left: none; border-right: none;">si</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')" required data-provide="datepicker" class="form-control datepicker" name="perioada_sfarsit" id="perioada_sfarsit" placeholder="Incheierea livrarii">
        </div>
    </div>
</div>

            <div class="form-group row">
              <label for="incepere" class="col-sm-3 form-control-label">Data inceperii livrarii</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  data-provide="datepicker" class="form-control datepicker" name="incepere" id="incepere" placeholder="  Data inceperii livrarii">
              </div>
            </div>
            <div class="form-group row">
              <label for="termen" class="col-sm-3 form-control-label">Termen de plata dorit</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="termen" id="termen" placeholder="Termen de plata dorit">
              </div>
            </div>
            <div class="form-group row">
              <label for="modalitate" class="col-sm-3 form-control-label">Modalitate de plata dorita</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="modalitate" id="modalitate" placeholder="Modalitate de plata dorita">
              </div>
            </div>
            <div class="form-group row">
              <label for="program" class="col-sm-3 form-control-label">Program de lucru <br> (optional)</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="program" id="program" placeholder="Program de lucru (optional)">
              </div>
            </div>
            <div class="form-group row">
              <label for="consum_locuri" class="col-sm-3 form-control-label">Locurile de consum <br>
 (optional - daca se cunoaste)</label>
              <div class="col-sm-9">
                <input type="file" name="consum_locuri" id="consum_locuri">
              </div>
            </div>
            <div class="form-group row">
              <label for="consum_anual" class="col-sm-3 form-control-label">Consumul anual defalcat pe luni <br>
(optional - daca se cunoaste)</label>
              <div class="col-sm-9">
                <input type="file" name="consum_anual" id="consum_anual">
              </div>
            </div>
            <div class="form-inline form-group row">
              <label class="col-sm-3 form-control-label">Data si ora de incepere a licitatiei</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  data-provide="datepicker" class="form-control datepicker-top" name="data_incepere_licitatie" id="data_incepere_licitatie" placeholder="  Data incepere">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="time" class="form-control" name="ora_incepere_licitatie" id="ora_incepere_licitatie">
              </div>
            </div>
            <div class="form-inline form-group row" id="data_si_ora_expirare">
              <label class="col-sm-3 form-control-label">Data si ora de expirare a licitatiei</label>
              <div class="col-sm-9">
                <input data-provide="datepicker" class="form-control datepicker-top" name="data_expirare_licitatie" id="data_expirare_licitatie" placeholder="  Data expirare">
                <input type="time" class="form-control" name="ora_expirare_licitatie" id="ora_expirare_licitatie">
              </div>
            </div>
            <div id="alte_container" class="form-group row" style="display: none">
              <label for="alte" class="col-sm-3 form-control-label">Descriere scurta <br> (optional)</label>
              <div class="col-sm-9">
                <textarea name="alte" id="alte" class="form-control"></textarea>
              </div>
            </div>
            <div id="descriere_lunga_container" class="form-group row" style="display: none">
              <label for="descriere_lunga" class="col-sm-3 form-control-label">Descriere lunga <br> (optional)</label>
              <div class="col-sm-9">
                <textarea name="descriere_lunga" id="descriere_lunga" class="form-control"></textarea>
              </div>
            </div>
            <button type="button" id="toggle_alte" class="btn btn-default">Adauga o descriere scurta</button>
            <button type="button" id="toggle_descriere_lunga" class="btn btn-default">Adauga o descriere lunga</button>
            <hr>
            @if ($utilizatori)
                <button type="submit" class="btn btn-default">Trimite</button>
            @else
                <button type="submit" class="btn btn-danger" disabled>Trimite</button>
                <p><span class="text-danger"><strong>Nu exista initiatori!</strong></span> Trebuie sa existe initiatori in baza de date pentru a adauga o licitatie.</p>
            @endif
          </form>
        </div>
      </div>
    </div>

<script type="text/javascript">
$('#toggle_alte').click(function () {
    $(this).hide();
    $('#alte_container').show();
});
$('#toggle_descriere_lunga').click(function () {
    $(this).hide();
    $('#descriere_lunga_container').show();
});
$('#alte').summernote();
$('#descriere_lunga').summernote();
$('#contains-checkbox > input[type="checkbox"]:nth-child(2)').click( function () {
    $("#data_expirare_licitatie").prop('required', !this.checked);
    $("#ora_expirare_licitatie").prop('required', !this.checked);
    $("#data_si_ora_expirare").toggle(!this.checked);
    $(".show_if_live").toggle(this.checked)
});
</script>
@endsection
