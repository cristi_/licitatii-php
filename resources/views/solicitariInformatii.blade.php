@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Solicitari de modificare a informatiilor
        </div>
        <table class="table table-striped table-hover">
                @forelse ($solicitari as $solicitare)
                <tr><td style="padding: 10px;">
<div class="pull-left"><strong>#{{$solicitare->id}}</strong> <a href="membru/{{$solicitare->id_user}}">{{$solicitare->nume_user}}</a> a solicitat 

@if ($solicitare->actiune == 'licitatie')
<a href="{{$solicitare->link}}">modificarea informatiilor acestei licitatii</a>
@elseif ($solicitare->actiune == 'personal')
<a href="{{$solicitare->link}}">modificarea informatiilor personale</a>
@elseif ($solicitare->actiune == 'initiator')
<a href="{{$solicitare->link}}">modificarea informatiilor de initiator</a>
@elseif ($solicitare->actiune == 'participare')
<a href="{{$solicitare->link}}">participarea la aceasta licitatie</a>
@else
<a href="{{$solicitare->link}}">{{ $solicitare->actiune }}</a>
@endif

 in data de {{ date("d M Y H:i:s", strtotime(str_replace('-', '/', $solicitare->created_at))) }}</div>


<div class="pull-right"><a href="/sterge_solicitare/{{ $solicitare->id }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i><a></div>
<div class="clearfix"></div>
                </td></tr>
                @empty
                <tr><td>nu exista solicitari</td></tr>
                @endforelse
        </table>
      </div>
    </div>
@endsection
