@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        @if ($licitatie->aprobat)
            @if ($activa)
                <div class="panel panel-success">
            @else
                <div class="panel panel-default">
            @endif
        @else
            <div class="panel panel-warning">
        @endif
        <div class="panel-heading">
        <h4 class="titlu-licitatie"><abbr title="Aceasta licitatie este live" class="label label-danger">live</abbr></h4>
        <h3 class="titlu-licitatie">{{ $licitatie->denumire_licitatie }}</h3>
        @if ($licitatie->aprobat)
            @if ($activa)
                <span>- licitatie activa</span>
            @else
                <span>- licitatie inactiva</span>
            @endif
        @else
            <span>- licitatie in curs de aprobare</span>
        @endif
        <div class="pull-right">
            @if (Auth::user()->admin)
            @elseif (Auth::user()->name == $licitatie->denumire_initiator)
                    <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/edit" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile</a>
            @endif
                @if ($activa)
                    @if ($participant)
                        <a class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> esti participant</a>
                    @elseif ($participare_solicitata)
                        <a class="btn btn-info"><i class="glyphicon glyphicon-ok"></i> participarea a fost solicitata</a>
                    @elseif (Auth::user()->admin == 1 || Auth::user()->initiator == 1)
                    @else
                    <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_participant_solicitare" method="POST" class="form-inline" id="participant-name">
                        {!! csrf_field() !!}                
                        <button name="name" value="{{ Auth::user()->name }}" class="btn btn-primary" type="sunmit"><i class="glyphicon glyphicon-plus"></i> participa la licitatie</button>
                    </form>
                @endif
            @endif
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="meniu-licitatie" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="glyphicon glyphicon-option-vertical"></i></button>
                <ul class="dropdown-menu" aria-labelledby="meniu-licitatie">
                    <li class="dropdown-header">Vizualizare</li>
                    <li id="toggle-lista-licitatii">
                        <a class="btn btn-default">
                            <i class="fa fa-eye-slash"></i>
                            <span class="lista-licitatii-ascuns" style="display: none;">arata lista de licitatii active</span>
                            <span class="lista-licitatii-vizibil">ascunde lista de licitatii active</span>
                        </a>
                    </li>
                    <li id="toggle-prezentare">
                        <a class="btn btn-default">
                            <i class="fa fa-eye-slash"></i>
                            <span class="prezentare-ascuns" style="display: none;">arata prezentarea scurta</span>
                            <span class="prezentare-vizibil">ascunde prezentarea scurta</span>
                        </a>
                    </li>
                    <li id="toggle-chat">
                        <a class="btn btn-default">
                            <i class="fa fa-eye-slash"></i>
                            <span class="chat-ascuns" style="display: none;">arata chatul</span>
                            <span class="chat-vizibil">ascunde chatul</span>
                        </a>
                    </li>
                    @if ($participant)
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Optiuni participant</li>
                        <li>
                            <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/0" class="btn btn-default"><i class="glyphicon glyphicon-remove"></i> paraseste licitatia</a>
                        </li>
                    @endif
                    @if ($initiator)
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Optiuni admin</li>
                        <li>
                            <a class="btn btn-default">
                                <i class="fa fa-eye-slash"></i>
                                <span class="lista-licitatii-ascuns" style="display: none;">arata chatul</span>
                                <span class="lista-licitatii-vizibil">ascunde chatul</span>
                            </a>
                        </li>
                    @endif
                    @if (Auth::user()->admin)
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Optiuni admin</li>
                        <li>
                            <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/edit" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile</a>
                        </li>
                        <li>
                            <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_participant" method="POST" class="form-inline" id="participant-name">
                                {!! csrf_field() !!}                
                                <div class="input-group input-group-sm">
                                    <select name="name" class="form-control">
                                        @foreach ($utilizatori as $utilizator)
                                            <option value="{{$utilizator->name}}">{{$utilizator->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="sunmit"><i class="glyphicon glyphicon-plus"></i> adauga participant</button>
                                    </span>
                                </div>
                            </form>
                        </li>
                        @if ($licitatie->aprobat)
                        <li>
                            <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/0" class="btn btn-default"><i class="glyphicon glyphicon-remove"></i> dezaproba licitatia</a>
                        </li>
                        @else
                        <li>
                            <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/1" class="btn btn-default"><i class="glyphicon glyphicon-ok"></i> aproba licitatia</a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/delete" class="btn btn-default"><i class="glyphicon glyphicon-trash"></i> sterge licitatia</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

<table class="table table-condensed">
<tr class="">
    <th><i class="fa fa-btn fa-clock-o"></i>Ora serverului</th>
    <th>Ora inceperii licitatiei</th>
    <th>Durata etapei 1</th>
    <th>Durata etapei 2</th>
</tr>
<tr class="">
    <td><span id="time"></span>
</td>
    <td>{{ $licitatie->ora_incepere_licitatie }}
@if (Auth::user()->admin)
        <button class="btn btn-default btn-xs" title="editeaza ora de incepere"><i class="glyphicon glyphicon-pencil"></i></button>
@endif
</td>
    <td>{{ $licitatie->durata_etapa_1 }} minute
@if (Auth::user()->admin)
        <button class="btn btn-default btn-xs" title="editeaza durata etapei"><i class="glyphicon glyphicon-pencil"></i></button>
@endif
</td>
    <td>{{ $licitatie->durata_etapa_2 }} minute
@if (Auth::user()->admin)
        <button class="btn btn-default btn-xs" title="editeaza durata etapei"><i class="glyphicon glyphicon-pencil"></i></button>
@endif
</td>
</tr>
</table>
<br>


<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default right-corners-left sectiune-ascunde-row sectiune-refresh" id="sectiune-lista-licitatii">
            <div class="panel-heading">
                <span class="pull-left">Licitatii active astazi</span>
                <span class="pull-right">
                    <button class="btn btn-default btn-xs" title="reimprospateaza lista"><i class="glyphicon glyphicon-refresh"></i></button>
                    <button class="btn btn-danger btn-xs ascunde" title="ascunde sectiunea"><i class="glyphicon glyphicon-remove"></i></button>
                </span>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body table table-hover tall-panel">
                <ol class="list-unstyled list-inline">
                    @foreach ($licitatii as $l)
                    <li>
                        <span>{{ $l->id }}.</span> <a href="{{ url('/licitatie') }}/{{$licitatie->id}}" class="center-inline">{{$licitatie->denumire_licitatie}}</a>
                        @if ($l->live)
                        <abbr title="Aceasta licitatie este live" class="label label-danger pull-right">live</abbr>
                        @else
                        <abbr title="Aceasta licitatie este live" class="label label-primary pull-right">la termen</abbr>
                        @endif
                    </li>
                    @endforeach
                </ol>
            </div>
            <div class="panel-footer">
                <a href="{{ url('/licitatii') }}" class="btn btn-default btn-block">lista completa licitatii</a>
            </div>
        </div>
    </div>
<div class="col-md-9">
<!
/*************************************************************************************************************
 * SECTIE: rezultat; PRIVILEGIU NECESAR: membru
 *************************************************************************************************************/
>
@if ($licitatie->aprobat)
    @if (Auth::user()->admin)
        <script src="/js/admin_oferte_socket.js"></script>
    @else 
        <script src="/js/normal_oferte_socket.js"></script>
    @endif
    @if ($rezultat)
        @if ($oferte != '')
            @if (!Auth::user()->admin && !$participant && !$initiator )
                <div class="bs-callout bs-callout-success row-margin">
                    <h4>Licitatia a luat sfarsit</h4>
                    Oferta castigatoare este: {{ $castigator['suma'] }}.
                </div>
            @else
                <div class="bs-callout bs-callout-success row-margin">
                    <h4>Licitatia a luat sfarsit</h4>
                    Participantul castigator este {{ $castigator['nume'] }} cu oferta de {{ $castigator['suma'] }}.
                </div>
            @endif
        @endif
    @endif
@endif <!-- ($licitatie->aprobat) -->
<div class="bs-callout bs-callout-info sectiune-ascunde row-margin" id="sectiune-prezentare">
    <h4>
        Prezentare scurta
        <span class="pull-right">
            @if (Auth::user()->admin)
                <button class="btn btn-default btn-md" title="editeaza descrierea"><i class="glyphicon glyphicon-pencil"></i></button>
            @endif
            <button class="btn btn-danger btn-md ascunde" title="ascunde sectiunea"><i class="glyphicon glyphicon-remove"></i></button>
        </span>
    </h4>
    {!! $licitatie->alte !!}
</div>
<div class="row">
    @if ((!$participant || !$initiator || !Auth::user()->admin) && !$activa)
        <div class="col-md-12">
    @else
        <div class="col-md-6">
    @endif
        <table class="table">
        <tr class="info table-bordered">
            <th>Initiatorul licitatiei
            </th>
            <th>Cantitatea (in MWh)
            </th>
        </tr>
        <tr class="table-bordered">
            <td>{{ $licitatie->denumire_initiator }}
@if (Auth::user()->admin)
                <button class="btn btn-default btn-xs" title="schimba initiatorul"><i class="glyphicon glyphicon-pencil"></i></button>
@endif
            </td>
            <td>{{ $licitatie->cantitate }}
@if (Auth::user()->admin)
                <button class="btn btn-default btn-xs" title="editeaza cantitatea"><i class="glyphicon glyphicon-pencil"></i></button>
@endif
            </td>
        </tr>
        </table>
    </div>
    @if (($participant || $initiator || Auth::user()->admin) && $activa)
    <div class="col-md-6">
        <table class="table">
        <tr class="danger table-bordered">
            <th>Furnizor</th>
            <th>Pretul introdus</th>
            <th>Actiuni</th>
        </tr>
<!
/*************************************************************************************************************
 * SECTIE: oferte; PRIVILEGIU NECESAR: membru
 *************************************************************************************************************/
>
@if ($oferte != '')
    @if (Auth::user()->admin || $initiator)
        @forelse ($participanti as $participant_object)
            @if (Auth::user()->admin)
                <tr class="table-bordered">
                    <script src="/js/admin_oferte_sterge_participant.js"></script>
                    @forelse ($oferte as $oferta)
                        <script src="/js/admin_oferte_sterge_oferta.js"></script>
                        @if ($oferta['nume'] == $participant_object->name)
                            <td>
                                <a href="{{ url('/membru') .'/'. $oferta['id'] }}">{{ $oferta['nume'] }}</a>
                            </td>
                            <td>{{ $oferta['suma'] }}</td>
                        @else
                            <td>
                                <a href="{{ url('/membru') .'/'. $participant_object->id }}">{{ $participant_object->name }}</a>
                            </td>
                            <td>oferta inexistenta</td>
                        @endif
                        <td>
                            <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_object->id }}">
                                {!! csrf_field() !!}                
                                <button name="name" value="{{ $participant_object->name }}" class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge participant</button>
                            </form>
                            <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta" method="POST" class="" id="admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}">
                                {!! csrf_field() !!}
                                <input type="text" name="data" value = "{{ $oferta['data'] }}" style="display:none">
                                <input type="text" name="id" value = "{{ $oferta['id'] }}" style="display:none">
                                <button class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> anuleaza oferta</button>
                            </form>
                            <div class="clearfix"></div>
                        </td>
                    @empty
                        <td>
                            <a href="{{ url('/membru') .'/'. $participant_object->id }}">{{ $participant_object->name }}</a>
                        </td>
                        <td>oferta inexistenta</td>
                        <td>
                            <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_object->id }}">
                                {!! csrf_field() !!}                
                                <button name="name" value="{{ $participant_object->name }}" class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge participant</button>
                            </form>
                            <div class="clearfix"></div>
                        </td>
                    @endforelse
                </tr>
                <!-- SFARSIT SECTIE ADMIN -->
            @else <!-- (Auth::user->admin) -->
            @endif
        @empty
            <tr class="table-bordered">
                <td colspan="42">nu exista participanti</td>
            </tr>
        @endforelse
    @endif
    @if ($participant)
        <tr class="table-bordered">
            @forelse ($oferte as $oferta)
                <td>{{ Auth::user()->name }}</td>
                <td>{{ $oferta['suma'] }}</td>
                <td>
                    @if ($activa)
                        <button id="adauga_oferta_popover_btn" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-pencil"></i> modifica oferta</button>
                    @endif
                </td>
                @if (Auth::user()->name != $oferta['nume'])
                    @if ($oferta['nume'] == Auth::user()->name)
                    <td>{{ $oferta['nume'] }}</td>
                    @else
                    <td#>{{ hash('crc32b', $oferta['nume'].$licitatie->date_created) }}</td>
                    @endif
                    <td>{{ $oferta['suma'] }}</td>
                    <td></td>
                @endif
            @empty
                <td>{{ Auth::user()->name }}</td>
                <td>nu ai introdus oferta</td>
                <td>
                    @if ($activa)
                        <button id="adauga_oferta_popover_btn" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-plus"></i> adauga oferta</button>
                    @endif
                </td>
            @endif
        </tr>
        <div id="camp-adauga-oferta" style="display:none" class="panel panel-default">
            <div class="panel-heading titlebar">Adauga ofera
                <div class="pull-right">
                    <button id="oferta-popover-close" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="panel-body">
                <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_oferta" method="POST" class="form-group" id="participant-oferte">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input 
                            oninvalid="this.setCustomValidity(this.willValidate?'':'Va rugam sa folosit virgula ca separator')" 
                            title="Introduceti un numar pana la doua decimale folosind virgula ca separator." 
                            data-url="{{url('/licitatie') }}/{{ $licitatie->id }}/adauga_oferta" 
                            pattern="^([1-9][0-9]+|[1-9])+(\,\d{1,2})?" 
                            class="form-control" 
                            autocomplete="off"
                            placeholder="suma" 
                            id="oferta" 
                            name="suma" 
                            type="text" 
                        />
                    </div>
                    <button type="submit" id="buton-oferta" class="btn btn-default btn-block">Adauga oferta</button>
                    <div id="error" hidden class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Va rugam sa introduceti un numar valid pana la doua decimale si folosit virgula ca separator.</div>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endif
</table>
@endif
</div> <!-- .row -->
</div> <!-- .col-md-x -->
<!
/*************************************************************************************************************
 * SECTIE: chat live; PRIVILEGIU NECESAR: participant, initiator sau admin
 *************************************************************************************************************/
>
@if ($participant || $initiator || Auth::user()->admin)
    <div class="row row-margin">
        <div class="col-md-6">
            <div id="chat-live" class="panel panel-default">
                <div class="panel-heading">
                <div class="pull-right">
                    <button class="btn btn-danger btn-xs" title="ascunde chatul"><i class="glyphicon glyphicon-remove"></i></button>
                </div>
                    <form action="" class="">
                        <label for="chat-live-input">Chat live</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Mesajul tau">
                            <span class="input-group-btn">
                                <input type="button" value="Trimite" class="btn btn-default" id="chat-live-input">
                            </span>
                        </div>
                    </form>
                </div>
                <div class="panel-body" id="chat-panel-body">
                    <ul id="chat" class="list-unstyled">
                        <li>
                            <div class="message-body">
                                <div class="message-header">
                                    <strong>username</strong>
                                    <small class="pull-right text-muted">
                                        <i class="glyphicon glyphicon-time"></i>
                                        acum 10 minute
                                    </small>
                                </div>
                                <p class="message-content">
                                    Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="message-body">
                                <div class="message-header">
                                    <strong>username</strong>
                                    <small class="pull-right text-muted">
                                        <i class="glyphicon glyphicon-time"></i>
                                        acum 10 minute
                                    </small>
                                </div>
                                <p class="message-content">
                                    Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        </div>
    </div>
@endif
<!
/*************************************************************************************************************
 * SECTIE: solicitari de participare la licitatie; PRIVILEGIU NECESAR: adimn
 *************************************************************************************************************/
>
@if (Auth::user()->admin)
    <div class="panel-body">
        <hr>
        <section style="max-width: 400px">
        <h3>Solicitari de participare:</h3>
        @if ($participanti_solicitare != '')
            <ol>
            @foreach ($participanti_solicitare as $participant_solicitare_object)
                <li><a href="{{ url('/membru') .'/'. $participant_solicitare_object->id }}">{{ $participant_solicitare_object->name }}</a>
                    <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant_solicitare" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_solicitare_object->id }}">
                        {!! csrf_field() !!}                
                            <button name="name" value="{{ $participant_solicitare_object->name }}" class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge solicitarea</button>
                    </form>
                    <span class="pull-right" style="margin: 1px 5px;"></span>
                    <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/aproba_participant_solicitare" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_solicitare_object->id }}">
                        {!! csrf_field() !!}                
                        <button name="name" value="{{ $participant_solicitare_object->name }}" class="btn btn-success btn-xs" type="sunmit"><i class="glyphicon glyphicon-ok"></i> aproba</button>
                    </form>
                    <div class="clearfix"></div>
                </li>
                <br>
            @endforeach
            </ol>
            @else
                <p>nu exista solicitari de participare</p>
            @endif
        </section>
    </div>
@endif
</div>
</div>
</div>
</div>
@endsection
