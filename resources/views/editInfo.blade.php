@extends('layouts.app')

@section('content')
<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
      <form role="form" method="POST" action="{{ url('/editeaza_informatiile') }}">
        {!! csrf_field() !!}
        <textarea id="summernote" name="info">
          {{ $info }}
        </textarea>

        <input type="submit" class="btn btn-default" value="Salveaza">
      </form>
    </div>
  </div>
</div>
<script>
$('#summernote').summernote({
  focus: true,
  height: 300
});
</script>

@endsection
