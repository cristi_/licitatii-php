              <h3>Oferte:</h3>
              @if ($oferte != '')
              @foreach ($oferte as $oferta)
              ({{ date("h:i:s",$oferta['data']) }}) <a href="{{ url('/membru') .'/'. $oferta['id'] }}">{{ $oferta['nume'] }}</a> a oferit suma: <strong>{{ $oferta['suma'] }}</strong></a>
              
              @if (Auth::user()->admin)
              <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta" method="POST" class="pull-right" id="admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}">
                {!! csrf_field() !!}
                <input type="text" name="data" value = "{{ $oferta['data'] }}" style="display:none">
                <input type="text" name="id" value = "{{ $oferta['id'] }}" style="display:none">
                <button class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge oferta</button>
              </form>
              <div class="clearfix"></div>
<script type="text/javascript">

$("#admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}").submit(function(e) {

  var url = "{{url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta"; // the script where you handle the form input.
  $.ajax({
         type: "POST",
         url: url,
         data: $("#admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}").serialize(), // serializes the form's elements.
         success: function(data)
         {
          try {
            socket.send('update');
          } catch (e) {
            console.log(e);
          }
              $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/admin-oferte");
         }
       });

  e.preventDefault(); // avoid to execute the actual submit of the form.
});
</script>
              @endif

              <br>

              @endforeach
              @else
              <p>nu exista oferte</p>
              @endif
              <hr>
