@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">
      @if ($licitatie->aprobat)
      <div class="panel panel-default">
        @else
        <div class="panel panel-warning">
          @endif
          @if (Auth::guest())
          <div class="panel-heading">
            <a title="Trebuie sa fi logat pentru a participa" disabled="disabled" class="btn btn-default pull-right"><i class="glyphicon glyphicon-plus"></i> participa la licitatie</a>
            <span class="">Detalii licitatie</span>
            <div class="clearfix"></div>
          </div>
          <table role="table" class="table table-condensed table-bordered panel-body">
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Denumire licitatie</strong></td>
              <td class="col-sm-9">{{ $licitatie->denumire_licitatie }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Denumire initiator</strong></td>
              <td class="col-sm-9">{{ $licitatie->denumire_initiator }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Cantitate estimata in MWh</strong></td>
              <td class="col-sm-9">{{ $licitatie->cantitate }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Nivel de tensiune</strong></td>
              <td class="col-sm-9">{{ $licitatie->tensiune }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Perioada de livrare</strong></td>
              <td class="col-sm-9">intre {{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->perioada_start))) }} si {{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->perioada_sfarsit))) }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Data inceperii livrarii</strong></td>
              <td class="col-sm-9">{{ $licitatie->incepere }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Termen de plata dorit</strong></td>
              <td class="col-sm-9">{{ $licitatie->termen }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Modalitate de plata dorita</strong></td>
              <td class="col-sm-9">{{ $licitatie->modalitate }}</td>
            </tr>
            @if ($licitatie->program)
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Program de lucru</strong></td>
              <td class="col-sm-9">{{ $licitatie->program }}</td>
            </tr>
            @endif
            @if ($licitatie->alte)
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Alte informatii</strong></td>
              <td class="col-sm-9" style="word-wrap break-word">{{ $licitatie->alte }}</td>
            </tr>
            @endif
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Locuri de consum</strong></td>
              <td class="col-sm-9"><a href="#" class="btn btn-sm btn-default">descarca document</a></td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Consumul anual</strong></td>
              <td class="col-sm-9"><a href="#" class="btn btn-sm btn-default">descarca document</a></td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Data si ora inceperii licitatiei</strong></td>
              <td class="col-sm-9">{{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->data_incepere_licitatie))) }}, {{ $licitatie->ora_incepere_licitatie }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Data si ora expirarii licitatiei</strong></td>
              <td class="col-sm-9">{{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->data_expirare_licitatie))) }}, {{ $licitatie->ora_expirare_licitatie }}</td>
            </tr>
          </table>
          @else
          <div class="panel-heading">
            @if ($licitatie->aprobat)
            @if ($licitatie->live)
            <h4 style="display: inline;"><abbr title="Aceasta licitatie este live" class="label label-danger" alt="test">live</abbr></h4>
            @endif
            @if ($activa)
            <span>Detalii licitatie - activa</span>
            @else
            <span>Detalii licitatie - inactiva</span>
            @endif
            @else
            <span>Detalii licitatie - in curs de aprobare</span>
            @endif
            @if (Auth::user()->admin)
            <div class="pull-right">
              <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/edit" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile</a>
              <button class="btn btn-default btn-sm" type="button" id="adauga-participant"><i class="glyphicon glyphicon-user"></i> adauga participant</button>
              @if ($licitatie->aprobat)
              <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/0" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-remove"></i> dezaproba</a>
              @else
              <a href="{{ url('/aproba_licitatie') }}/{{$licitatie->id}}/1" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-ok"></i> aproba</a>
              @endif
              <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/delete" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i> sterge licitatia</a>
              <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_participant" method="POST" class="form-inline" id="participant-name">
                {!! csrf_field() !!}                
                <br>
                <div class="input-group">
                  <select name="name" class="form-control">
                    @foreach ($utilizatori as $utilizator)
                    <option value="{{$utilizator->name}}">{{$utilizator->name}}</option>
                    @endforeach
                  </select>
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="sunmit"><i class="glyphicon glyphicon-plus"></i> adauga</button>
                  </span>
                </div>
              </form>
            </div>
            <script type="text/javascript">
            $('#participant-name').toggle();
            $("#adauga-participant").click(function(){
              $('#participant-name').toggle();
            });
            </script>
            @elseif (Auth::user()->name == $licitatie->denumire_initiator)
            <div class="pull-right">
              <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/edit" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> editeaza informatiile</a>
            </div>
            <script type="text/javascript">
            $('#participant-name').toggle();
            $("#adauga-participant").click(function(){
              $('#participant-name').toggle();
            });
            </script>
            @endif
            <div class="pull-right">
              @if ($activa)
              @if ($participant)
              <a class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> esti participant</a>
              @elseif ($participare_solicitata)
              <a class="btn btn-info"><i class="glyphicon glyphicon-ok"></i> participarea a fost solicitata</a>
              @elseif (Auth::user()->admin == 1 || Auth::user()->initiator == 1)
              @else
              <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_participant_solicitare" method="POST" class="form-inline" id="participant-name">
                {!! csrf_field() !!}                
                <button name="name" value="{{ Auth::user()->name }}" class="btn btn-primary" type="sunmit"><i class="glyphicon glyphicon-plus"></i> participa la licitatie</button>
              </form>
              @endif
              @endif
            </div>
            <div class="clearfix"></div>
          </div>
          <table role="table" class="table table-condensed table-bordered panel-body">
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Denumire licitatie</strong></td>
              <td class="col-sm-9">{{ $licitatie->denumire_licitatie }}</td>
            </tr>
            @if (Auth::user()->admin)
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Denumire initiator</strong></td>
              <td class="col-sm-9"><a href="{{ url('/membru/'.$id_initiator) }}">{{ $licitatie->denumire_initiator }}</a></td>
            </tr>
            @else
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Denumire initiator</strong></td>
              <td class="col-sm-9">{{ $licitatie->denumire_initiator }}</td>
            </tr>
            @endif
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Cantitate estimata in MWh</strong></td>
              <td class="col-sm-9">{{ $licitatie->cantitate }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Nivel de tensiune</strong></td>
              <td class="col-sm-9">{{ $licitatie->tensiune }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Perioada de livrare</strong></td>
              <td class="col-sm-9">intre {{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->perioada_start))) }} si {{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->perioada_sfarsit))) }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Data inceperii livrarii</strong></td>
              <td class="col-sm-9">{{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->incepere))) }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Termen de plata dorit</strong></td>
              <td class="col-sm-9">{{ $licitatie->termen }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Modalitate de plata dorita</strong></td>
              <td class="col-sm-9">{{ $licitatie->modalitate }}</td>
            </tr>
            @if ($licitatie->program)
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Program de lucru</strong></td>
              <td class="col-sm-9">{{ $licitatie->program }}</td>
            </tr>
            @endif
            @if ($licitatie->alte)
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Prezentare scurta</strong></td>
              <td class="col-sm-9" style="word-wrap break-word">{{ $licitatie->alte }}</td>
            </tr>
            @endif
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Lista locuri de consum</strong></td>
              <td class="col-sm-9"><a class="btn btn-default btn-sm" href="{{ url($licitatie->consum_locuri) }}" download>Descarca fisier</a></td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Consumuri anuale estimate</strong></td>
              <td class="col-sm-9"><a class="btn btn-default btn-sm" href="{{ url($licitatie->consum_anual) }}" download>Descarca fisier</a></td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Data si ora inceperii licitatiei</strong></td>
              <td class="col-sm-9">{{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->data_incepere_licitatie))) }}, {{ $licitatie->ora_incepere_licitatie }}</td>
            </tr>
            <tr class="row">
              <td class="col-sm-3"><strong class="pull-right">Data si ora expirarii licitatiei</strong></td>
              <td class="col-sm-9">{{ date("d M Y", strtotime(str_replace('-', '/', $licitatie->data_expirare_licitatie))) }}, {{ $licitatie->ora_expirare_licitatie }}</td>
            </tr>
          </table>
          @endif

@if ($licitatie->aprobat)

          @if ($licitatie->live)
          @if (Auth::user()->admin)
          <script type="text/javascript">
          var host   = 'ws://176.223.125.107:8891';
          var socket = null;

                try {
                  socket = new WebSocket(host);
                  
                  //Manages the open event within your client code
                  socket.onopen = function () {
                      console.log('Connection Opened');
                      return;
                  };
                  //Manages the message event within your client code
                  socket.onmessage = function (msg) {
                      console.log(msg.data);
                        $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/admin-oferte");
                      return;
                  };
                  //Manages the close event within your client code
                  socket.onclose = function () {
                      console.log('Connection Closed');
                      return;
                  };
              } catch (e) {
                  console.log(e);
              }

          </script>
          @else 
          <script type="text/javascript">
          var host   = 'ws://176.223.125.107:8891';
          var socket = null;

                try {
                  socket = new WebSocket(host);
                  
                  //Manages the open event within your client code
                  socket.onopen = function () {
                      console.log('Connection Opened');
                      return;
                  };
                  //Manages the message event within your client code
                  socket.onmessage = function (msg) {
                      console.log(msg.data);
                        $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/participant-oferte");
                      return;
                  };
                  //Manages the close event within your client code
                  socket.onclose = function () {
                      console.log('Connection Closed');
                      return;
                  };
              } catch (e) {
                  console.log(e);
              }

          </script>
          @endif

          <div class="panel-body">

            @if ($rezultat)
            <section>
              @if ($oferte != '')
              @if (!Auth::user()->admin && !$participant && !$initiator )
              <h3>Oferta castigatoare este: {{ $castigator['suma'] }}</h3>
@else
              <h3>Participantul castigator este {{ $castigator['nume'] }} cu oferta urmatoare: {{ $castigator['suma'] }}</h3>
              @endif
              @endif
            </section>
            @endif


            @if ($participant || $initiator || Auth::user()->admin)
            <section style="max-width: 400px" id="oferte">
              <h3>Oferte:</h3>
              @if ($oferte != '')
              @foreach ($oferte as $oferta)
              @if (Auth::user()->admin)
              ({{ date("h:i:s",$oferta['data']) }}) <a href="{{ url('/membru') .'/'. $oferta['id'] }}">{{ $oferta['nume'] }}</a> a oferit suma: <strong>{{ $oferta['suma'] }}</strong></a>
              @else
                  @if ($oferta['nume'] == Auth::user()->name)
                  <mark>
                  ({{ date("h:i:s",$oferta['data']) }}) tu oferit suma: <strong>{{ $oferta['suma'] }}</strong></a>
                </mark>
                  @else
                  ({{ date("h:i:s",$oferta['data']) }}) Furnizor #{{ hash('crc32b', $oferta['nume']) }} a oferit suma: <strong>{{ $oferta['suma'] }}</strong></a>
                  @endif
              @endif
              
              @if (Auth::user()->admin)
              <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta" method="POST" class="pull-right" id="admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}">
                {!! csrf_field() !!}
                <input type="text" name="data" value = "{{ $oferta['data'] }}" style="display:none">
                <input type="text" name="id" value = "{{ $oferta['id'] }}" style="display:none">
                <button class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge oferta</button>
              </form>
              <div class="clearfix"></div>
<script type="text/javascript">

$("#admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}").submit(function(e) {

  var url = "{{url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta"; // the script where you handle the form input.
  $.ajax({
         type: "POST",
         url: url,
         data: $("#admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}").serialize(), // serializes the form's elements.
         success: function(data)
         {
          try {
            socket.send('update');
          } catch (e) {
            console.log(e);
          }
              $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/admin-oferte");
         }
       });

  e.preventDefault(); // avoid to execute the actual submit of the form.
});
</script>
              @endif

              <br>

              @endforeach
              @else
              <p>nu exista oferte</p>
              @endif
              <hr>
            </section>
@endif



            @if (Auth::user()->admin)

            <section style="max-width: 400px">
              <h3>Participanti:</h3>
              @if ($participanti != '')
              <ol>
                @foreach ($participanti as $participant_object)
                <li><a href="{{ url('/membru') .'/'. $participant_object->id }}">{{ $participant_object->name }}</a>

                  <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_object->id }}">
                    {!! csrf_field() !!}                
                    <button name="name" value="{{ $participant_object->name }}" class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge participant</button>
                  </form>
                  <div class="clearfix"></div>
<script type="text/javascript">

$("#admin-sterge-participant{{ $participant_object->id }}").submit(function(e) {

    try {
    socket.send('update');
    } catch (e) {
    console.log(e);
    }

            //$('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/admin-oferte");
  /*var url = "{{url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant"; // the script where you handle the form input.
  $.ajax({
         type: "POST",
         url: url,
         data: $("#admin-sterge-participant{{ $participant_object->id }}").serialize(), // serializes the form's elements.
         success: function(data)
         {
          try {
            socket.send('update');
          } catch (e) {
            console.log(e);
          }
         }
       });
   */
  //e.preventDefault(); // avoid to execute the actual submit of the form.
});
</script>

                </li>
                <br>
                @endforeach
              </ol>
              @else
              <p>nu exista participanti</p>
              @endif
            </section>
            
            @elseif ($participant && $activa)

            <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_oferta" method="POST" class="form-group" id="participant-oferte">
              {!! csrf_field() !!}
              <div class="form-inline">
      <!--          <input type="number" step="any" class="form-control" placeholder="suma" name="suma" id="oferta">
                <button type="submit" class="btn btn-primary">Adauga oferta</button> -->
  <input placeholder="suma" class="form-control" oninvalid="this.setCustomValidity(this.willValidate?'':'Va rugam sa folosit virgula ca separator')" type="text" pattern="^([1-9][0-9]+|[1-9])+(\,\d{1,2})?" name="suma" title="Introduceti un numar pana la doua decimale folosind virgula ca separator." id="oferta"/>
<button type="submit" id="buton-oferta" class="btn btn-primary">
    Adauga oferta
  </button>
<div id="error" hidden class="alert alert-danger">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
Va rugam sa introduceti un numar valid pana la doua decimale si folosit virgula ca separator.</div>
</div>
              </div>
            </form>

<script type="text/javascript">

$('#oferta').on('change keyup', function(event) {
    if ( event.target.validity.valid ) {
                $('#error').hide();
        $('#buton-oferta').addClass('btn-default');
        $('#buton-oferta').removeClass('btn-danger');
        $('#buton-oferta').prop('disabled', false);

    } else {
        $('#buton-oferta').prop('disabled', true);
        $('#buton-oferta').removeClass('btn-default');
        $('#buton-oferta').addClass('btn-danger');
        $('#error').show();
    }    
});

$("#participant-oferte").submit(function(e) {

if ($('#oferta').val().length > 0){
  var url = "{{url('/licitatie') }}/{{ $licitatie->id }}/adauga_oferta"; // the script where you handle the form input.

  $.ajax({
         type: "POST",
         url: url,
         data: $("#participant-oferte").serialize(), // serializes the form's elements.
         success: function(data)
         {
          try {
            socket.send('update');
          } catch (e) {
            console.log(e);
          }
          $('#oferte').load("{{ url('/licitatie') }}/{{ $licitatie->id }}/participant-oferte");
         }
       });
}
  e.preventDefault(); // avoid to execute the actual submit of the form.
  document.getElementById('participant-oferte').reset();
});

</script>

            @elseif ($initiator)

            <section>
              <h3>Participanti:</h3>
              @if ($participanti != '')
              <ol>
                @foreach ($participanti as $participant_object)
                <li>{{ $participant_object->name }}</li>
                @endforeach
              </ol>
              @else
              <p>nu exista participanti</p>
              @endif
            </section>
            @endif

          </div>

          @else <!-- daca licitatia este la termen -->

          @if (Auth::user()->admin)
          <div class="panel-body">

            @if ($rezultat)
            <section>
              @if ($oferte != '')
              <h3>Participantul castigator este <a href="{{ url('/membru') .'/'. $castigator['id'] }}">{{ $castigator['nume'] }}</a> cu oferta urmatoare: {{ $castigator['suma'] }}</h3>
              @endif
            </section>
            @endif

            <section style="max-width: 400px">
              <h3>Oferte:</h3>
              @if ($oferte != '')
              <ol>
                @foreach ($oferte as $oferta)
                <li>
                  <a href="{{ url('/membru') .'/'. $oferta['id'] }}">{{ $oferta['nume'] }}</a>: <strong>{{ $oferta['suma'] }}</strong></a>
                  <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_oferta" method="POST" class="pull-right" id="admin-sterge-oferta{{ $oferta['id'] . $oferta['data'] }}">
                    {!! csrf_field() !!}
                    <input type="text" name="data" value = "{{ $oferta['data'] }}" style="display:none">
                    <input type="text" name="id" value = "{{ $oferta['id'] }}" style="display:none">
                    <button class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge oferta</button>
                  </form>
                  <div class="clearfix"></div>
                </li>
<br>

                @endforeach
              </ol>
              @else
              <p>nu exista oferte</p>
              @endif
            </section>

            <section style="max-width: 400px">
              <h3>Participanti:</h3>
              @if ($participanti != '')
              <ol>
                @foreach ($participanti as $participant_object)
                <li><a href="{{ url('/membru') .'/'. $participant_object->id }}">{{ $participant_object->name }}</a>

                  <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant" method="POST" class="pull-right">
                    {!! csrf_field() !!}                
                    <button name="name" value="{{ $participant_object->name }}" class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge participant</button>
                  </form>
                  <div class="clearfix"></div>

                </li>
<br>

                @endforeach
              </ol>
              @else
              <p>nu exista participanti</p>
              @endif
            </section>

          </div>
          @endif

          @if ($participant && $activa)
          <div class="panel-body">
            <section><!-- array_search(Auth::user()->id, json_decode($licitatie->oferte, true)) -->
              <h3>Oferta ta: {{ $oferte == '' ? 'nu ai depus oferta' : $oferte[0]['suma'] }}
              </h3>
            </section>
            <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/adauga_oferta" method="POST" class="form-group">
              {!! csrf_field() !!}
              <div class="form-inline">
                <input type="number" class="form-control" placeholder="suma" name="suma">
                <button type="submit" class="btn btn-primary">Actualizeaza oferta</button>
              </div>
            </form>
          </div>

          @endif

          @if ($initiator)
          <div class="panel-body">

            @if ($rezultat)
            <section>
              @if ($oferte != '')
              <h3>Participantul castigator este {{ $castigator['nume'] }} cu oferta urmatoare: {{ $castigator['suma'] }}</h3>
              @endif
            </section>
            @endif

            <section>
              <h3>Participanti:</h3>
              @if ($participanti != '')
              <ol>
                @foreach ($participanti as $participant_object)
                <li>{{ $participant_object->name }}</li>
                @endforeach
              </ol>
              @else
              <p>nu exista participanti</p>
              @endif
            </section>

            <section>
              <h3>Oferte:</h3>
              @if ($oferte != '')
              <ol>
                @foreach ($oferte as $oferta)
                <li>{{ $participanti[array_search($oferta['id'], array_column(json_decode(json_encode($participanti), true), 'id'))]->name }}: <strong>{{ $oferta['suma'] }}</strong></a></li>
                @endforeach
              </ol>
              @else
              <p>nu exista oferte</p>
              @endif
            </section>

          </div>
          @endif

          @endif

        @if (Auth::user()->admin)
        
        <div class="panel-body">
        <hr>
            <section style="max-width: 400px">
            <h3>Solicitari de participare:</h3>
              @if ($participanti_solicitare != '')
              <ol>
                @foreach ($participanti_solicitare as $participant_solicitare_object)
                <li><a href="{{ url('/membru') .'/'. $participant_solicitare_object->id }}">{{ $participant_solicitare_object->name }}</a>

                  <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/sterge_participant_solicitare" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_solicitare_object->id }}">
                    {!! csrf_field() !!}                
                    <button name="name" value="{{ $participant_solicitare_object->name }}" class="btn btn-danger btn-xs" type="sunmit"><i class="glyphicon glyphicon-remove"></i> sterge solicitarea</button>
                  </form>
<span class="pull-right" style="margin: 1px 5px;"></span>
                  <form action="{{ url('/licitatie') }}/{{ $licitatie->id }}/aproba_participant_solicitare" method="POST" class="pull-right" id="admin-sterge-participant{{ $participant_solicitare_object->id }}">
                    {!! csrf_field() !!}                
                    <button name="name" value="{{ $participant_solicitare_object->name }}" class="btn btn-success btn-xs" type="sunmit"><i class="glyphicon glyphicon-ok"></i> aproba</button>
                  </form>
                  <div class="clearfix"></div>

                </li>
                <br>
                @endforeach
              </ol>
              @else
              <p>nu exista solicitari de participare</p>
              @endif
            </section>
        </div>
        @endif
@endif

        </div>
      </div>
  @endsection
