@extends('layouts.app')

@section('content')
<div class="col-md-4">
  <div class="panel panel-default">
    <div class="panel-heading">
      <span class="pull-left">Cele mai recente licitatii</span>
      <div class="clearfix"></div>
    </div>
    <table class="panel-body table table-hover table-responsive">
      <thead>
        <th>#</th>
        <th>denumire licitatie</th>
        <th></th>
      </thead>
      <tbody>
        @foreach ($licitatii as $licitatie)
        <tr>
          <td>{{ $licitatie->id }}</td>
          <td><a href="{{ url('/licitatie') }}/{{$licitatie->id}}">{{$licitatie->denumire_licitatie}}</a></td>
          <td>
            @if ($licitatie->live)
            <abbr title="Aceasta licitatie este live" class="label label-danger" alt="test">live</abbr>
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="panel-footer">
      <a href="{{ url('/licitatii') }}" class="btn btn-default btn-block">lista licitatii</a>
    </div>
  </div>
</div>
<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-heading">
      <span class="pull-left">Informatii</span>
      @if (Auth::user())
      @if (Auth::user()->admin)
      <div class="pull-right">
        <a href="/editeaza_informatiile" class="btn btn-default">Editeaza</a>
      </div>
      @endif
      @endif
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      {!! $info[0]->info !!}
    </div>
  </div>
</div>
@endsection
