@extends('layouts.app')

@section('content')
@if (Auth::user() && Auth::user()->admin)
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Adauga utilizator</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label class="col-md-4 control-label">Nume</label>

              <div class="col-md-6">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label class="col-md-4 control-label">Email</label>

              <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label class="col-md-4 control-label">Parola</label>

              <div class="col-md-6">
                <input type="password" class="form-control" name="password">

                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              <label class="col-md-4 control-label">Confirma parola</label>

              <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation">

                @if ($errors->has('password_confirmation'))
                <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Adresa</label>

              <div class="col-md-6">
                <input type="text" class="form-control" name="adresa" value="{{ old('adresa') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">CUI</label>

              <div class="col-md-6">
                <input type="text" class="form-control" name="cui" value="{{ old('cui') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Persoana de contact</label>

              <div class="col-md-6">
                <input type="text" class="form-control" name="contact" value="{{ old('contact') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Telefon</label>

              <div class="col-md-6">
                <input type="text" class="form-control" name="telefon" value="{{ old('telefon') }}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Alte informatii</label>

              <div class="col-md-6">
                <textarea class="form-control" name="alte">{{ old('alte') }}</textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label"></label>

              <div class="col-md-6">
                <label>
                  {!! Form::hidden('initiator', false) !!}
                  {!! Form::checkbox('initiator', true) !!} Initiator de licitatii
                </label>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-btn fa-user"></i>Inregistreaza utilizator
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    @else
    neautorizat
    @endif
@endsection
