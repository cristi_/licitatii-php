@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">

@if ($initiator->solicitare)
      <div class="panel panel-warning" id="panel-solicitare">
<div class="panel-heading">
@if (Auth::user()->admin)
        Modificarile solicitate de utilizatorul {{ $initiator->denumire }}
        <div class="pull-right">
            <button class="btn btn-default buton-solicitare">Arata informatiile originale</button>
            <a href="{{ url('/membru') }}/{{ $initiator->user_id }}/stergeSolicitareaInitiator" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Sterge solicitarea</a>
        </div>
        <div class="clearfix"></div>
@else
        Modificarile solicitate sunt in curs de aprobare
        <div class="pull-right">
            <a href="{{ url('/membru') }}/{{ $initiator->user_id }}/stergeSolicitareaInitiator" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Sterge solicitarea</a>
        </div>
        <div class="clearfix"></div>
@endif
</div>

          <div class="panel-body">
@if ($success)
<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
@if (Auth::user()->admin)
        <strong>succes!</strong> detaliile au fost modificate.
@else
        <strong>succes!</strong> detaliile au fost solicitate.
@endif
</div>
@endif


      @if (Auth::user()->admin)
      <form role="form" enctype="multipart/form-data" method="POST" action="{{ url('/membru/') }}/{{ $initiator->user_id }}/aprobaInitiator">
        @else
          <form role="form" enctype="multipart/form-data" method="POST" action="{{ url('/membru/') }}/{{ $initiator->user_id }}/editInitiator">
          @endif
                        {!! csrf_field() !!}

        <div class="form-group row">
            <label for="denumire" class="col-sm-3 form-control-label">Denumire initiator <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input readonly type="text" class="form-control" name="denumire" id="denumire" value="{{$initiator->denumire}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="cantitate" class="col-sm-3 form-control-label">Cantitate estimata in MWh <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu cantitatea estimata')" required type="text" class="form-control" name="cantitate" id="cantitate" value="{{$initiator->solicitare_cantitate}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="nivel" class="col-sm-3 form-control-label">Nivel de tensiune <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu nivelul de tensiune')" required type="text" class="form-control" name="nivel" id="nivel" value="{{$initiator->solicitare_nivel}}">
          </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Perioada de livrare <span class="text-danger">*</span></label>
          <div class="col-sm-9">
          <div class="input-daterange input-group">
    <span class="input-group-addon">intre</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu data inceperii perioadei de livrare')" required data-provide="datepicker" class="form-control datepicker" name="perioada_start" id="perioada_start" value="{{$initiator->solicitare_perioada_start}}">
    <span class="input-group-addon" style="border-left: none; border-right: none;">si</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu data incheierii perioadei de livrare')" required data-provide="datepicker" class="form-control datepicker" name="perioada_sfarsit" id="perioada_sfarsit" value="{{$initiator->solicitare_perioada_sfarsit}}">
          </div>
          </div>
        </div>

        <div class="form-group row">
            <label for="incepere" class="col-sm-3 form-control-label">Data inceperi livrarii <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu data inceperii livrarii')" required data-provide="datepicker" class="form-control datepicker" name="incepere" id="incepere" value="{{$initiator->solicitare_incepere}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="termen" class="col-sm-3 form-control-label">Termen de plata dorit <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu termenul de plata dorit')" required type="text" class="form-control" name="termen" id="termen" value="{{$initiator->solicitare_termen}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="modalitate" class="col-sm-3 form-control-label">Modalitate de plata dorita <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu modalitatea de plata')" required type="text" class="form-control" name="modalitate" id="modalitate" value="{{$initiator->solicitare_modalitate}}">
          </div>
        </div>
        @if ($initiator->solicitare_program)
        <div class="form-group row">
            <label for="program" class="col-sm-3 form-control-label">Program de lucru</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="program" id="program" value="{{$initiator->solicitare_program}}">
          </div>
        </div>
        @else
        <div class="form-group row">
            <label for="program" class="col-sm-3 form-control-label">Program de lucru</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="program" id="program" placeholder="program">
          </div>
        </div>
        @endif
        <div class="form-group row">
          <label for="consum_locuri" class="col-sm-3">Lista locuri de consum</label>
<div class="col-sm-9">
          <input type="file" name="consum_locuri" id="consum_locuri">
</div>
        </div>
        <div class="form-group row">
          <label for="consum_anual" class="col-sm-3">Consumuri anuale estimate</label>
<div class="col-sm-9">
          <input type="file" name="consum_anual" id="consum_anual">
</div>
        </div>
          @if (Auth::user()->admin)
          <input type="submit" value="Aproba modificarile" class="btn btn-default">
          @else 
          <input type="submit" value="Solicita modificarea" class="btn btn-default">
          @endif
      </form>
<br>
<hr>
<p><span class="text-danger">*</span><span class="text-info"> aceste campuri sunt necesare</span></p>


          </div>
      </div>
@endif

    @if ( !$initiator->solicitare || ( Auth::user()->admin && $initiator->solicitare ))
@if (Auth::user()->admin && $initiator->solicitare)
  <div class="panel panel-default" id="panel-original" style="display:none">
@else
  <div class="panel panel-default" id="panel-original">
@endif
    <div class="panel-heading">
        Modificare detalli initiator {{ $initiator->denumire }}
@if (Auth::user()->admin && $initiator->solicitare)
        <div class="pull-right">
            <button class="btn btn-default buton-solicitare">Arata modificarile solicitate</button>
        </div>
        <div class="clearfix"></div>
@endif
        </div>

          <div class="panel-body">
@if ($success)
<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
@if (Auth::user()->admin)
        <strong>succes!</strong> detaliile au fost modificate.
@else
        <strong>succes!</strong> detaliile au fost solicitate.
@endif
</div>
@endif


      <form role="form" enctype="multipart/form-data" method="POST" action="{{ url('/membru/') }}/{{ $initiator->user_id }}/editInitiator">
                        {!! csrf_field() !!}

        <div class="form-group row">
            <label for="denumire" class="col-sm-3 form-control-label">Denumire initiator <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input readonly type="text" class="form-control" name="denumire" id="denumire" value="{{$initiator->denumire}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="cantitate" class="col-sm-3 form-control-label">Cantitate estimata in MWh <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu cantitatea estimata')" required type="text" class="form-control" name="cantitate" id="cantitate" value="{{$initiator->cantitate}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="nivel" class="col-sm-3 form-control-label">Nivel de tensiune <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu nivelul de tensiune')" required type="text" class="form-control" name="nivel" id="nivel" value="{{$initiator->nivel}}">
          </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Perioada de livrare <span class="text-danger">*</span></label>
          <div class="col-sm-9">
          <div class="input-daterange input-group">
    <span class="input-group-addon">intre</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu data inceperii perioadei de livrare')" required data-provide="datepicker" class="form-control datepicker" name="perioada_start" id="perioada_start" value="{{$initiator->perioada_start}}">
    <span class="input-group-addon" style="border-left: none; border-right: none;">si</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu data incheierii perioadei de livrare')" required data-provide="datepicker" class="form-control datepicker" name="perioada_sfarsit" id="perioada_sfarsit" value="{{$initiator->perioada_sfarsit}}">
          </div>
          </div>
        </div>

        <div class="form-group row">
            <label for="incepere" class="col-sm-3 form-control-label">Data inceperi livrarii <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu data inceperii livrarii')" required data-provide="datepicker" class="form-control datepicker" name="incepere" id="incepere" value="{{$initiator->incepere}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="termen" class="col-sm-3 form-control-label">Termen de plata dorit <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu termenul de plata dorit')" required type="text" class="form-control" name="termen" id="termen" value="{{$initiator->termen}}">
          </div>
        </div>

        <div class="form-group row">
            <label for="modalitate" class="col-sm-3 form-control-label">Modalitate de plata dorita <span class="text-danger">*</span></label>
          <div class="col-sm-9">
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'introdu modalitatea de plata')" required type="text" class="form-control" name="modalitate" id="modalitate" value="{{$initiator->modalitate}}">
          </div>
        </div>
        @if ($initiator->program)
        <div class="form-group row">
            <label for="program" class="col-sm-3 form-control-label">Program de lucru</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="program" id="program" value="{{$initiator->program}}">
          </div>
        </div>
        @else
        <div class="form-group row">
            <label for="program" class="col-sm-3 form-control-label">Program de lucru</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="program" id="program" placeholder="program">
          </div>
        </div>
        @endif
        <div class="form-group row">
          <label for="consum_locuri" class="col-sm-3">Lista locuri de consum</label>
<div class="col-sm-9">
          <input type="file" name="consum_locuri" id="consum_locuri">
</div>
        </div>
        <div class="form-group row">
          <label for="consum_anual" class="col-sm-3">Consumuri anuale estimate</label>
<div class="col-sm-9">
          <input type="file" name="consum_anual" id="consum_anual">
</div>
        </div>
          @if (Auth::user()->admin)
          <input type="submit" value="Salveaza" class="btn btn-default">
          @else 
          <input type="submit" value="Solicita modificarea" class="btn btn-default">
          @endif
      </form>
<br>
<hr>
<p><span class="text-danger">*</span><span class="text-info"> aceste campuri sunt necesare</span></p>


          </div>
      </div>
@endif
<script>
$('.buton-solicitare').click(function () {
    $('#panel-original').toggle();
    $('#panel-solicitare').toggle();
});
</script>

    </div>
@endsection
