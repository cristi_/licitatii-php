@extends('layouts.app')

@section('content')
    <div class="col-md-10 col-md-offset-1">

@if ($licitatie->solicitare)
  <div class="panel panel-warning" id="panel-solicitare">
    <div class="panel-heading">
@if (Auth::user()->admin)
        Modificarile solicitate de utilizatorul {{ $licitatie->denumire_initiator }}
        <div class="pull-right">
            <button class="btn btn-default buton-solicitare">Arata informatiile originale</button>
            <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/stergeSolicitare" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Sterge solicitarea</a>
        </div>
        <div class="clearfix"></div>
@else
        Modificarile solicitate sunt in curs de aprobare
        <div class="pull-right">
            <a href="{{ url('/licitatie') }}/{{ $licitatie->id }}/stergeSolicitare" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Sterge solicitarea</a>
        </div>
        <div class="clearfix"></div>
@endif
</div>

        <div class="panel-body">
      @if (Auth::user()->admin)
      <form role="form" enctype="multipart/form-data" method="POST" action="{{ url('/licitatie') }}/{{ $licitatie->id }}/aproba">
        @else
          <form role="form" enctype="multipart/form-data" methodr"POST" action="{{ url('/licitatie') }}/{{ $licitatie->id }}/edit">
          @endif

            {!! csrf_field() !!}
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Licitatie live </label>
              <div class="col-sm-9">
                @if ($licitatie->solicitare_live) 
                {!! Form::hidden('live', false) !!}
                {!! Form::checkbox('live', 1, true) !!}
                @else
                {!! Form::hidden('live', false) !!}
                {!! Form::checkbox('live', true) !!}
                @endif
              </div>
            </div>
@if (Auth::user()->admin || $licitatie->aprobat)
            <div class="form-group row">
              <label for="denumire_licitatie" class="col-sm-3 form-control-label">Denumire licitatie</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="denumire_licitatie" id="denumire_licitatie" value="{{$licitatie->solicitare_denumire_licitatie}}">
              </div>
            </div>
@endif
@if (Auth::user()->admin)
            <div class="form-group row">
              <label for="denumire_initiator" class="col-sm-3 form-control-label">Initiator</label>
              <div class="col-sm-9">
                  <select name="denumire_initiator" class="form-control">
                    @foreach ($utilizatori as $utilizator)
                    @if ($utilizator->name == $licitatie->denumire_initiator)
                        <option value="{{$utilizator->name}}" selected="selected">{{$utilizator->name}}</option>
                    @else
                        <option value="{{$utilizator->name}}">{{$utilizator->name}}</option>
                    @endif
                    @endforeach
                  </select>
              </div>
            </div>
@endif
            <div class="form-group row">
              <label for="cantitate" class="col-sm-3 form-control-label">Cantitate estimata in MWh</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="cantitate" id="cantitate" value="{{$licitatie->solicitare_cantitate}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="tensiune" class="col-sm-3 form-control-label">Nivel de tensiune</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="tensiune" id="tensiune" value="{{$licitatie->solicitare_tensiune}}">
              </div>
            </div>
        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Perioada de livrare</label>
          <div class="col-sm-9">
          <div class="input-daterange input-group">
    <span class="input-group-addon">intre</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')" required data-provide="datepicker" class="form-control datepicker" name="perioada_start" id="perioada_start" value="{{$licitatie->perioada_start}}">
    <span class="input-group-addon" style="border-left: none; border-right: none;">si</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')" required data-provide="datepicker" class="form-control datepicker" name="perioada_sfarsit" id="perioada_sfarsit" value="{{$licitatie->perioada_sfarsit}}">
          </div>
          </div>
        </div>
            <div class="form-group row">
              <label for="incepere" class="col-sm-3 form-control-label">Data inceperii livrarii</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  data-provide="datepicker" class="form-control datepicker" name="incepere" id="incepere" value="{{ $licitatie->solicitare_incepere }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="termen" class="col-sm-3 form-control-label">Termen de plata dorit</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="termen" id="termen" value="{{$licitatie->solicitare_termen}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="modalitate" class="col-sm-3 form-control-label">Modalitate de plata dorita</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="modalitate" id="modalitate" value="{{$licitatie->solicitare_modalitate}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="program" class="col-sm-3 form-control-label">Program de lucru (optional)</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="program" id="program" value="{{$licitatie->solicitare_program}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="consum_locuri" class="col-sm-3 form-control-label">Locurile de consum (optional)</label>
              <div class="col-sm-9">
                <input type="file" name="consum_locuri" id="consum_locuri">
              </div>
            </div>
            <div class="form-group row">
              <label for="consum_anual" class="col-sm-3 form-control-label">Consumul anual defalcat pe luni (optional)</label>
              <div class="col-sm-9">
                <input type="file" name="consum_anual" id="consum_anual">
              </div>
            </div>
            <div class="form-inline form-group row">
              <label class="col-sm-3 form-control-label">Data si ora de incepere a licitatiei</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  data-provide="datepicker" class="form-control datepicker-top" name="data_incepere_licitatie" value="{{ $licitatie->solicitare_data_incepere_licitatie }}">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="time" class="form-control" name="ora_incepere_licitatie" value="{{$licitatie->solicitare_ora_incepere_licitatie}}">
              </div>
            </div>
            <div class="form-inline form-group row">
              <label class="col-sm-3 form-control-label">Data si ora de expirare a licitatiei</label>
              <div class="col-sm-9">
                <input data-provide="datepicker" class="form-control datepicker-top" name="data_expirare_licitatie" value="{{ $licitatie->solicitare_data_expirare_licitatie }}">
                <input type="time" class="form-control" name="ora_expirare_licitatie" value="{{$licitatie->solicitare_ora_expirare_licitatie}}">
              </div>
            </div>
            <div id="alte_container" class="form-group row">
              <label for="alte" class="col-sm-3 form-control-label">Descriere scurta <br> (optional)</label>
              <div class="col-sm-9">
                <textarea name="alte" id="solicitare_alte" class="form-control">{{$licitatie->solicitare_alte}}</textarea>
              </div>
            </div>
            <div id="descriere_lunga_container" class="form-group row">
              <label for="descriere_lunga" class="col-sm-3 form-control-label">Descriere lunga <br> (optional)</label>
              <div class="col-sm-9">
                <textarea name="descriere_lunga" id="solicitare_descriere_lunga" class="form-control">{{$licitatie->solicitare_descriere_lunga}}</textarea>
              </div>
            </div>
            <hr>
              @if (Auth::user()->admin)
              <input type="submit" value="Aproba modificarile" class="btn btn-default">
              @else 
              <input type="submit" value="Solicita modificarea" class="btn btn-default">
              @endif
          </form>
</div>
</div>
@endif



@if (!$licitatie->solicitare || ( Auth::user()->admin && $licitatie->solicitare ) )
@if (( Auth::user()->admin && $licitatie->solicitare ) )
  <div class="panel panel-default" id="panel-original" style="display:none">
@else
  <div class="panel panel-default" id="panel-original">
@endif
    <div class="panel-heading">
@if (substr($licitatie->denumire_licitatie, 0, strlen('Solicitare licitatie')) === 'Solicitare licitatie')
@if ($licitatie->aprobat)
        Modificare licitatie
@else
        Modificare licitatie solicitata
@endif
@else
        Modificare licitatie {{ $licitatie->denumire_licitatie }}
@endif
@if (Auth::user()->admin && $licitatie->solicitare)
        <div class="pull-right">
            <button class="btn btn-default buton-solicitare">Arata modificarile solicitate</button>
        </div>
        <div class="clearfix"></div>
@endif
</div>

        <div class="panel-body">
          <form role="form" enctype="multipart/form-data" method="POST" action="{{ url('/licitatie') }}/{{ $licitatie->id }}/edit">
            {!! csrf_field() !!}
            <div class="form-group row">
              <label class="col-sm-3 form-control-label">Licitatie live </label>
              <div class="col-sm-9" id="contains-checkbox">
                @if ($licitatie->live) 
                {!! Form::hidden('live', false) !!}
                {!! Form::checkbox('live', 1, true) !!}
                @else
                {!! Form::hidden('live', false) !!}
                {!! Form::checkbox('live', true) !!}
                @endif
              </div>
            </div>
@if (Auth::user()->admin || $licitatie->aprobat)
            <div class="form-group row">
              <label for="denumire_licitatie" class="col-sm-3 form-control-label">Denumire licitatie</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="denumire_licitatie" id="denumire_licitatie" value="{{$licitatie->denumire_licitatie}}">
              </div>
            </div>
@endif
@if (Auth::user()->admin)
@if ($licitatie->live)
            <div class="show_if_live show_etape form-group row">
@else
            <div class="show_if_live form-group row">
@endif
              <label class="col-sm-3 form-control-label" for="">Durata etapelor in minute</label>
              <div class="col-sm-9">
                <div class="input-daterange input-group">
                    <span class="input-group-addon">prima etapa:</span>
                    <input type="number" class="form-control" name="durata_etapa_1" value="10">
                    <span class="input-group-addon" style="border-left: none; border-right: none;">a doua etapa:</span>
                    <input type="number" class="form-control" name="durata_etapa_2" value="10">
                </div>
              </div>
          </div>
            <div class="form-group row">
              <label for="denumire_initiator" class="col-sm-3 form-control-label">Initiator</label>
              <div class="col-sm-9">
                  <select name="denumire_initiator" class="form-control">
                    @foreach ($utilizatori as $utilizator)
                    @if ($utilizator->name == $licitatie->denumire_initiator)
                        <option value="{{$utilizator->name}}" selected="selected">{{$utilizator->name}}</option>
                    @else
                        <option value="{{$utilizator->name}}">{{$utilizator->name}}</option>
                    @endif
                    @endforeach
                  </select>
              </div>
            </div>
@endif
            <div class="form-group row">
              <label for="cantitate" class="col-sm-3 form-control-label">Cantitate estimata in MWh</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="cantitate" id="cantitate" value="{{$licitatie->cantitate}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="tensiune" class="col-sm-3 form-control-label">Nivel de tensiune</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="tensiune" id="tensiune" value="{{$licitatie->tensiune}}">
              </div>
            </div>
        <div class="form-group row">
            <label class="col-sm-3 form-control-label">Perioada de livrare</label>
          <div class="col-sm-9">
          <div class="input-daterange input-group">
    <span class="input-group-addon">intre</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')" required data-provide="datepicker" class="form-control datepicker" name="perioada_start" id="perioada_start" value="{{$licitatie->perioada_start}}">
    <span class="input-group-addon" style="border-left: none; border-right: none;">si</span>
            <input oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')" required data-provide="datepicker" class="form-control datepicker" name="perioada_sfarsit" id="perioada_sfarsit" value="{{$licitatie->perioada_sfarsit}}">
          </div>
          </div>
        </div>
            <div class="form-group row">
              <label for="incepere" class="col-sm-3 form-control-label">Data inceperii livrarii</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  data-provide="datepicker" class="form-control datepicker-top" name="incepere" value="{{ $licitatie->incepere }}" id="incepere">
              </div>
            </div>
            <div class="form-group row">
              <label for="termen" class="col-sm-3 form-control-label">Termen de plata dorit</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="termen" id="termen" value="{{$licitatie->termen}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="modalitate" class="col-sm-3 form-control-label">Modalitate de plata dorita</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="text" class="form-control" name="modalitate" id="modalitate" value="{{$licitatie->modalitate}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="program" class="col-sm-3 form-control-label">Program de lucru (optional)</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="program" id="program" value="{{$licitatie->program}}">
              </div>
            </div>
            <div class="form-group row">
              <label for="consum_locuri" class="col-sm-3 form-control-label">Locurile de consum (optional)</label>
              <div class="col-sm-9">
                <input type="file" name="consum_locuri" id="consum_locuri">
              </div>
            </div>
            <div class="form-group row">
              <label for="consum_anual" class="col-sm-3 form-control-label">Consumul anual defalcat pe luni (optional)</label>
              <div class="col-sm-9">
                <input type="file" name="consum_anual" id="consum_anual">
              </div>
            </div>
            <div class="form-inline form-group row">
              <label class="col-sm-3 form-control-label">Data si ora de incepere a licitatiei</label>
              <div class="col-sm-9">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  data-provide="datepicker" class="datepicker-top form-control" name="data_incepere_licitatie" value="{{$licitatie->data_incepere_licitatie}}">
                <input required oninvalid="this.setCustomValidity(this.willValidate?'':'acest camp este necesar')"  type="time" class="form-control" name="ora_incepere_licitatie" value="{{$licitatie->ora_incepere_licitatie}}">
              </div>
            </div>
            <div class="form-inline form-group row">
              <label class="col-sm-3 form-control-label">Data si ora de expirare a licitatiei</label>
              <div class="col-sm-9">
                <input data-provide="datepicker" class="datepicker-top form-control" name="data_expirare_licitatie" value="{{$licitatie->data_expirare_licitatie}}">
                <input type="time" class="form-control" name="ora_expirare_licitatie" value="{{$licitatie->ora_expirare_licitatie}}">
              </div>
            </div>
            <div id="alte_container" class="form-group row">
              <label for="alte" class="col-sm-3 form-control-label">Descriere scurta <br> (optional)</label>
              <div class="col-sm-9">
                <textarea name="alte" id="alte" class="form-control">{{$licitatie->alte}}</textarea>
              </div>
            </div>
            <div id="descriere_lunga_container" class="form-group row">
              <label for="descriere_lunga" class="col-sm-3 form-control-label">Descriere lunga <br> (optional)</label>
              <div class="col-sm-9">
                <textarea name="descriere_lunga" id="descriere_lunga" class="form-control">{{$licitatie->descriere_lunga}}</textarea>
              </div>
            </div>
            <hr>
              @if (Auth::user()->admin)
              <input type="submit" value="Salveaza" class="btn btn-default">
              @else 
              <input type="submit" value="Solicita modificarea" class="btn btn-default">
              @endif
          </form>
</div>
</div>
@endif
<script>
$('#alte').summernote();
$('#descriere_lunga').summernote();
$('#solicitare_alte').summernote();
$('#solicitare_descriere_lunga').summernote();
$('.buton-solicitare').click(function () {
    $('#panel-original').toggle();
    $('#panel-solicitare').toggle();
});
$('#contains-checkbox > input[type="checkbox"]:nth-child(2)').click( function () {
    $(".show_if_live").toggle(this.checked)
});
</script>


    </div>
@endsection
