@extends('layouts.app')

@section('content')
<div class="col-md-10 col-md-offset-1">
@if ($membru->solicitare)
  <div class="panel panel-warning" id="panel-solicitare">
    <div class="panel-heading">
@if (Auth::user()->admin)
        Modificarile solicitate de utilizatorul {{ $membru->name }}
        <div class="pull-right">
            <button class="btn btn-default buton-solicitare">Arata informatiile originale</button>
            <a href="{{ url('/membru') }}/{{ $membru->id }}/stergeSolicitareaMembru" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Sterge solicitarea</a>
        </div>
        <div class="clearfix"></div>
@else
        Modificarile solicitate sunt in curs de aprobare
        <div class="pull-right">
            <a href="{{ url('/membru') }}/{{ $membru->id }}/stergeSolicitareaMembru" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Sterge solicitarea</a>
        </div>
        <div class="clearfix"></div>
@endif
    </div>
    <div class="panel-body">
      @if ($success)
      <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
@if (Auth::user()->admin)
        <strong>succes!</strong> detaliile au fost modificate.
@else
        <strong>succes!</strong> detaliile au fost solicitate.
@endif
      </div>
      @endif

          <div class="form-group row">
            <label for="name" class="col-sm-3 form-control-label">Nume</label>
            <div class="col-sm-9">
              <input readonly type="text" class="form-control" name="name" id="name" value="{{$membru->name}}">
            </div>
          </div>


      @if (Auth::user()->admin)
      <form role="form" method="POST" action="{{ url('/membru') }}/{{ $membru->id }}/aprobaMembru">
        @else
        <form role="form" method="POST" action="{{ url('/detalii') }}">
          @endif
          {!! csrf_field() !!}

          <div class="form-group row">
            <label for="email" class="col-sm-3 form-control-label">Email</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="email" id="email" value="{{$membru->solicitare_email}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="adresa" class="col-sm-3 form-control-label">Adresa</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="adresa" id="adresa" value="{{$membru->solicitare_adresa}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="cui" class="col-sm-3 form-control-label">CUI</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="cui" id="cui" value="{{$membru->solicitare_cui}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="contact" class="col-sm-3 form-control-label">Persoana de contact</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="contact" id="contact" value="{{$membru->solicitare_contact}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="telefon" class="col-sm-3 form-control-label">Telefon</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="telefon" id="telefon" value="{{$membru->solicitare_telefon}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="alte" class="col-sm-3 form-control-label">Alte informatii</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="alte" id="alte" >{{$membru->solicitare_alte}}</textarea>
            </div>
          </div>
          <hr>
          @if (Auth::user()->admin)
          <input type="submit" value="Aproba modificarile" class="btn btn-default">
          @else 
          <input type="submit" value="Solicita modificarea" class="btn btn-default">
          @endif
          </form>
      </div>
    </div>
    @endif
    @if ( !$membru->solicitare || ( Auth::user()->admin && $membru->solicitare ))
@if (Auth::user()->admin && $membru->solicitare)
  <div class="panel panel-default" id="panel-original" style="display:none">
@else
  <div class="panel panel-default" id="panel-original">
@endif
    <div class="panel-heading">
        Modificare detalli membru {{ $membru->name }}
@if (Auth::user()->admin && $membru->solicitare)
        <div class="pull-right">
            <button class="btn btn-default buton-solicitare">Arata modificarile solicitate</button>
        </div>
        <div class="clearfix"></div>
@endif
    </div>
    <div class="panel-body">
      @if ($success)
      <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
@if (Auth::user()->admin)
        <strong>succes!</strong> detaliile au fost modificate.
@else
        <strong>succes!</strong> detaliile au fost solicitate.
@endif
      </div>
      @endif

      @if (Auth::user()->admin)
      <form role="form" method="POST" action="{{ url('/membru') }}/{{ $membru->id }}/edit">
        @else
        <form role="form" method="POST" action="{{ url('/detalii') }}">
          @endif
          {!! csrf_field() !!}

          <div class="form-group row">
            <label for="name" class="col-sm-3 form-control-label">Nume</label>
            <div class="col-sm-9">
              <input readonly type="text" class="form-control" name="name" id="name" value="{{$membru->name}}">
            </div>
          </div>


          @if (Auth::user()->admin)
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Administrator </label>
            <div class="col-sm-9">
              @if ($membru->admin) 
              {!! Form::hidden('admin', false) !!}
              {!! Form::checkbox('admin', 1, true) !!}
              @else
              {!! Form::hidden('admin', false) !!}
              {!! Form::checkbox('admin', true) !!}
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Activ </label>
            <div class="col-sm-9">
              @if ($membru->activ) 
              {!! Form::hidden('activ', false) !!}
              {!! Form::checkbox('activ', 1, true) !!}
              @else
              {!! Form::hidden('activ', false) !!}
              {!! Form::checkbox('activ', true) !!}
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Initiator </label>
            <div class="col-sm-9">
              @if ($membru->initiator) 
              {!! Form::hidden('initiator', false) !!}
              {!! Form::checkbox('initiator', 1, true) !!}
              @else
              {!! Form::hidden('initiator', false) !!}
              {!! Form::checkbox('initiator', true) !!}
              @endif
            </div>
          </div>

          <div class="form-group row">
            <label for="name" class="col-sm-3 form-control-label">Parola</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="parola" id="name" placeholder="Introdu parola noua">
            </div>
          </div>

          @endif

          <div class="form-group row">
            <label for="email" class="col-sm-3 form-control-label">Email</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="email" id="email" value="{{$membru->email}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="adresa" class="col-sm-3 form-control-label">Adresa</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="adresa" id="adresa" value="{{$membru->adresa}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="cui" class="col-sm-3 form-control-label">CUI</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="cui" id="cui" value="{{$membru->cui}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="contact" class="col-sm-3 form-control-label">Persoana de contact</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="contact" id="contact" value="{{$membru->contact}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="telefon" class="col-sm-3 form-control-label">Telefon</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="telefon" id="telefon" value="{{$membru->telefon}}">
            </div>
          </div>

          <div class="form-group row">
            <label for="alte" class="col-sm-3 form-control-label">Alte informatii</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="alte" id="alte" >{{$membru->alte}}</textarea>
            </div>
          </div>
          <hr>
          @if (Auth::user()->admin)
          <input type="submit" value="Salveaza" class="btn btn-default">
          @else 
          <input type="submit" value="Solicita modificarea" class="btn btn-default">
          @endif
          </form>
      </div>
    </div>
@endif
<script>
$('.buton-solicitare').click(function () {
    $('#panel-original').toggle();
    $('#panel-solicitare').toggle();
});
</script>
  </div>
  @endsection
