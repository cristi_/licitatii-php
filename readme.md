pentru a instala laravel

    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
    sudo chmod +x /usr/local/bin/composer
    cd /var/www
    git clone https://github.com/laravel/laravel.git
    cd /var/www/laravel
    sudo composer install
    chown -R www-data.www-data /var/www/laravel
    chmod -R 755 /var/www/laravel
    php artisan key:generate

