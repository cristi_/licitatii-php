<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitiatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initiator', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('denumire');
            $table->string('cantitate');
            $table->string('nivel');
            $table->date('perioada_start');
            $table->date('perioada_sfarsit');
            $table->date('incepere');
            $table->string('termen');
            $table->string('modalitate');
            $table->string('program');
            $table->string('consum_locuri');
            $table->string('consum_anual');
            $table->timestamp('created_at');
            $table->boolean('solicitare');
            $table->string('solicitare_denumire');
            $table->string('solicitare_cantitate');
            $table->string('solicitare_nivel');
            $table->date('solicitare_perioada_start');
            $table->date('solicitare_perioada_sfarsit');
            $table->date('solicitare_incepere');
            $table->string('solicitare_termen');
            $table->string('solicitare_modalitate');
            $table->string('solicitare_program');
            $table->string('solicitare_consum_locuri');
            $table->string('solicitare_consum_anual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('initiator');
    }
}
