<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitariInformatiiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitari_informatii', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->string('nume_user');
            $table->string('link');
            $table->string('actiune');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitari_informatii');
    }
}
