<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicitatiiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licitatii', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_initiator')->unsigned()->nullable();
            $table->integer('durata_etapa_1');
            $table->integer('durata_etapa_2');
            $table->mediumText('participanti');
            $table->mediumText('participanti_solicitare');
            $table->mediumText('oferte');
            $table->boolean('live');
            $table->boolean('aprobat');
            $table->string('denumire_licitatie');
            $table->string('denumire_initiator');
            $table->string('cantitate');
            $table->string('tensiune');
            $table->date('perioada_start');
            $table->date('perioada_sfarsit');
            $table->date('incepere');
            $table->string('termen');
            $table->string('modalitate');
            $table->string('program');
            $table->mediumText('alte');
            $table->mediumText('descriere_lunga');
            $table->string('consum_locuri');
            $table->string('consum_anual');
            $table->date('data_incepere_licitatie');
            $table->time('ora_incepere_licitatie');
            $table->date('data_expirare_licitatie');
            $table->time('ora_expirare_licitatie');
            $table->timestamp('created_at');
            $table->boolean('solicitare');
            $table->boolean('solicitare_live');
            $table->string('solicitare_denumire_licitatie');
            $table->string('solicitare_cantitate');
            $table->string('solicitare_tensiune');
            $table->string('solicitare_perioada_start');
            $table->string('solicitare_perioada_sfarsit');
            $table->date('solicitare_incepere');
            $table->string('solicitare_termen');
            $table->string('solicitare_modalitate');
            $table->string('solicitare_program');
            $table->mediumText('solicitare_alte');
            $table->mediumText('solicitare_descriere_lunga');
            $table->string('solicitare_consum_locuri');
            $table->string('solicitare_consum_anual');
            $table->date('solicitare_data_incepere_licitatie');
            $table->time('solicitare_ora_incepere_licitatie');
            $table->date('solicitare_data_expirare_licitatie');
            $table->time('solicitare_ora_expirare_licitatie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('licitatii');
    }
}
