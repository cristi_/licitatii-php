<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('initiator');
            $table->boolean('admin');
            $table->boolean('activ');
            $table->string('name')->unique();
            $table->string('email');
            $table->string('password', 60);
            $table->mediumText('licitatii_initiator');
            $table->mediumText('licitatii_participant');
            $table->string('adresa');
            $table->string('cui');
            $table->string('contact');
            $table->string('telefon');
            $table->mediumText('alte');
            $table->rememberToken();
            $table->timestamps();
            $table->boolean('solicitare');
            $table->string('solicitare_email');
            $table->string('solicitare_adresa');
            $table->string('solicitare_cui');
            $table->string('solicitare_contact');
            $table->string('solicitare_telefon');
            $table->mediumText('solicitare_alte');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
