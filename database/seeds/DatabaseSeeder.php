<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(licitatiiSeeder::class);
        $this->call(usersSeeder::class);

        DB::table('info')->insert([
            'info' => 'test',
        ]);

        DB::table('solicitari')->insert([
            'solicitari_licitatii_num' => 0,
            'solicitari_informatii_num' => 0
        ]);
    }
}
