<?php

use Illuminate\Database\Seeder;

class licitatiiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
		for ($x = 0; $x <= 100; $x++) {
	        DB::table('licitatii')->insert([
	            'live' => rand(0, 1),
	            'aprobat' => rand(0, 1),
	            'denumire_licitatie' => str_random(5),
	            'denumire_initiator' => str_random(5),
	            'cantitate' => str_random(5),
	            'tensiune' => str_random(15),
	            'perioada' => str_random(5),
	            'incepere' =>  date('H:i:s',strtotime('2013-02-20 02:25:21')),
	            'termen' => str_random(5),
	            'modalitate' => str_random(5),
	            'program' => str_random(5),
	            'alte' => str_random(5) . ' ' .str_random(5),
	            'data_incepere_licitatie' =>  date('H:i:s',strtotime('2013-02-20 02:25:21')),
	            'ora_incepere_licitatie' =>  date('H:i:s',strtotime('2013-02-20 02:25:21')),
	            'data_expirare_licitatie' =>  date('H:i:s',strtotime('2013-02-20 02:25:21')),
	            'ora_expirare_licitatie' =>  date('H:i:s',strtotime('2013-02-20 02:25:21')),
        	]);
	    }
	    
    }
}
