<?php

use Illuminate\Database\Seeder;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = str_random(10);
        $file = fopen('pass.txt', 'w');
        fwrite($file, $pass);
        fclose($file);

        DB::table('users')->insert([
            'initiator' => false,
            'admin' => true,
            'activ' => true,
            'name' => 'admin',
            'email' => 'admin@admin.admin',
            'password' => bcrypt('testtest'),
        ]);

        
/*
		for ($x = 0; $x <= 100; $x++) {
	        DB::table('users')->insert([
                'initiator' => rand(0, 1),
                'admin' => rand(0, 1),
                'activ' => rand(0, 1),
                'name' => str_random(10),
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
                'adresa' => str_random(10),
                'cui' => str_random(10),
                'contact' => str_random(10),
                'telefon' => str_random(10),
                'alte' => str_random(10),
	        ]);
		} 
*/
        
    }
}
