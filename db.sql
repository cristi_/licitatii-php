-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `info` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` VALUES ('test','2016-03-03 15:48:34');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `initiator`
--

DROP TABLE IF EXISTS `initiator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `initiator` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `denumire` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perioada_start` date NOT NULL,
  `perioada_sfarsit` date NOT NULL,
  `incepere` date NOT NULL,
  `termen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modalitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `program` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consum_locuri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consum_anual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `solicitare` tinyint(1) NOT NULL,
  `solicitare_denumire` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_cantitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_nivel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_perioada_start` date NOT NULL,
  `solicitare_perioada_sfarsit` date NOT NULL,
  `solicitare_incepere` date NOT NULL,
  `solicitare_termen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_modalitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_program` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_consum_locuri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_consum_anual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `initiator_user_id_foreign` (`user_id`),
  CONSTRAINT `initiator_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `initiator`
--

LOCK TABLES `initiator` WRITE;
/*!40000 ALTER TABLE `initiator` DISABLE KEYS */;
INSERT INTO `initiator` VALUES (2,'initiator','','','0000-00-00','0000-00-00','0000-00-00','','','','','','2016-03-03 15:49:10',0,'','','','0000-00-00','0000-00-00','0000-00-00','','','','',''),(3,'furnizor','','','0000-00-00','0000-00-00','0000-00-00','','','','','','2016-03-03 15:49:24',0,'','','','0000-00-00','0000-00-00','0000-00-00','','','','',''),(4,'furnizor2','','','0000-00-00','0000-00-00','0000-00-00','','','','','','2016-03-03 15:49:33',0,'','','','0000-00-00','0000-00-00','0000-00-00','','','','','');
/*!40000 ALTER TABLE `initiator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licitatii`
--

DROP TABLE IF EXISTS `licitatii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licitatii` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_initiator` int(10) unsigned DEFAULT NULL,
  `durata_etapa_1` int(11) NOT NULL,
  `durata_etapa_2` int(11) NOT NULL,
  `participanti` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `participanti_solicitare` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `oferte` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `live` tinyint(1) NOT NULL,
  `aprobat` tinyint(1) NOT NULL,
  `denumire_licitatie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `denumire_initiator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tensiune` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perioada_start` date NOT NULL,
  `perioada_sfarsit` date NOT NULL,
  `incepere` date NOT NULL,
  `termen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modalitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `program` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alte` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `descriere_lunga` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `consum_locuri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consum_anual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_incepere_licitatie` date NOT NULL,
  `ora_incepere_licitatie` time NOT NULL,
  `data_expirare_licitatie` date NOT NULL,
  `ora_expirare_licitatie` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `solicitare` tinyint(1) NOT NULL,
  `solicitare_live` tinyint(1) NOT NULL,
  `solicitare_denumire_licitatie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_cantitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_tensiune` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_perioada_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_perioada_sfarsit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_incepere` date NOT NULL,
  `solicitare_termen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_modalitate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_program` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_alte` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_descriere_lunga` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_consum_locuri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_consum_anual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_data_incepere_licitatie` date NOT NULL,
  `solicitare_ora_incepere_licitatie` time NOT NULL,
  `solicitare_data_expirare_licitatie` date NOT NULL,
  `solicitare_ora_expirare_licitatie` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licitatii`
--

LOCK TABLES `licitatii` WRITE;
/*!40000 ALTER TABLE `licitatii` DISABLE KEYS */;
INSERT INTO `licitatii` VALUES (1,2,0,0,'[\"3\",\"4\"]','','[{\"data\":1457133794,\"id\":\"3\",\"suma\":\"21\"}]',1,1,'test','initiator','test','test','2016-03-02','2016-03-04','2016-03-03','test','test','test','<p>aaaa <b>aaaaa</b></p>','','files/licitatii/1_consum_locuri_install.sh','','2016-03-03','02:00:00','2016-03-05','02:00:00','2016-03-05 01:40:40',0,0,'','','','','','0000-00-00','','','','','','','','0000-00-00','00:00:00','0000-00-00','00:00:00'),(2,2,0,0,'','','',0,1,'border','initiator','border','border','2016-03-02','2016-03-04','2016-03-03','border','border','border','','','','','2016-03-03','01:00:00','2016-03-03','13:00:00','2016-03-03 17:32:32',0,0,'','','','','','0000-00-00','','','','','','','','0000-00-00','00:00:00','0000-00-00','00:00:00');
/*!40000 ALTER TABLE `licitatii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_01_19_073602_create_licitatii_table',1),('2016_01_19_074629_create_initiator_table',1),('2016_02_10_065428_create_info_table',1),('2016_02_20_123219_create_solicitari_table',1),('2016_02_22_075210_create_solicitari_informatii_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitari`
--

DROP TABLE IF EXISTS `solicitari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitari` (
  `solicitari_licitatii_num` int(11) NOT NULL,
  `solicitari_informatii_num` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitari`
--

LOCK TABLES `solicitari` WRITE;
/*!40000 ALTER TABLE `solicitari` DISABLE KEYS */;
INSERT INTO `solicitari` VALUES (0,0,'2016-03-03 15:48:34');
/*!40000 ALTER TABLE `solicitari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitari_informatii`
--

DROP TABLE IF EXISTS `solicitari_informatii`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitari_informatii` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nume_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actiune` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitari_informatii`
--

LOCK TABLES `solicitari_informatii` WRITE;
/*!40000 ALTER TABLE `solicitari_informatii` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitari_informatii` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `initiator` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `activ` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `licitatii_initiator` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `licitatii_participant` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `adresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cui` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alte` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `solicitare` tinyint(1) NOT NULL,
  `solicitare_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_adresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_cui` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_telefon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitare_alte` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,0,1,1,'admin','admin@admin.admin','$2y$10$9XosNBVZaUsEKmZWzMdlJe3tQrGOTwXKirHVxMxP5f9CbQ8idaNOq','','','','','','','',NULL,NULL,NULL,0,'','','','','',''),(2,1,0,1,'initiator','initiator@initiator.initiator','$2y$10$iek3p7js/8s26qyB5s6nxe7XZtF2WqGtgxNrpPTpc99HsOsEqPax.','','','','','','','',NULL,'2016-03-03 17:49:10','2016-03-03 17:49:10',0,'','','','','',''),(3,0,0,1,'furnizor','furnizor@furnizor.furnizor','$2y$10$Twp66TMGpxcGul6JQ37OfuOjgxpAYGzbIjFnsbnWmsHiHfYS.vwuK','','[\"1\"]','','','','','',NULL,'2016-03-03 17:49:24','2016-03-03 17:49:24',0,'','','','','',''),(4,0,0,1,'furnizor2','furnizor@furnizor.furnizor2','$2y$10$ifd9ifYGIl2Cr4tBqGYV8uLVkIo/CA984z9o2ZkfUXpZq2JjVVHHa','','[\"1\"]','','','','','',NULL,'2016-03-03 17:49:33','2016-03-03 17:49:33',0,'','','','','','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-05 11:18:45
